/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : db_shopsolution

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2020-08-04 08:10:59
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(45) NOT NULL,
  `created_at` varchar(45) NOT NULL,
  `updated_at` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO groups VALUES ('1', 'Administrator', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO groups VALUES ('5', 'Member', '2020-07-11 21:48:52', '2020-08-03 18:56:41');
INSERT INTO groups VALUES ('6', 'Acknowledge By', '2020-07-11 21:49:06', '');

-- ----------------------------
-- Table structure for `heading_menu`
-- ----------------------------
DROP TABLE IF EXISTS `heading_menu`;
CREATE TABLE `heading_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `sort_by` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of heading_menu
-- ----------------------------
INSERT INTO heading_menu VALUES ('1', 'Main', '1');
INSERT INTO heading_menu VALUES ('2', 'Admin', '2');
INSERT INTO heading_menu VALUES ('3', 'Setting', '3');

-- ----------------------------
-- Table structure for `image`
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `image_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of image
-- ----------------------------
INSERT INTO image VALUES ('1', '1', '7e2a9bfbd31fdda1a9b6814ff208bc9f.jpeg');
INSERT INTO image VALUES ('2', '1', '5039e6300a74bd3503f890890e927b68.jpeg');

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `head_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `link` varchar(150) NOT NULL,
  `icon` varchar(45) NOT NULL,
  `order_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO menus VALUES ('30', '3', '0', 'Setting', '#', 'fa-cog', null);
INSERT INTO menus VALUES ('31', '0', '30', 'Menu', 'setting/menu', 'fa-circle', null);
INSERT INTO menus VALUES ('34', '0', '30', 'Users', 'setting/users', 'fa-circle', null);
INSERT INTO menus VALUES ('44', '0', '30', 'User Groups', 'setting/group', 'fa-circle', null);
INSERT INTO menus VALUES ('69', '1', '0', 'Dashboard', 'dashboard', 'fa-crcle', null);
INSERT INTO menus VALUES ('77', '1', '0', 'Orders', '#', 'fa fa-tasks', null);
INSERT INTO menus VALUES ('78', '0', '77', 'Form', 'order/form', 'fa-circle', null);
INSERT INTO menus VALUES ('79', '0', '77', 'List Order', 'order/list', 'fa-circle', null);

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `link` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO orders VALUES ('1', '2', 'https://www.warungbelajar.com/membuat-fitur-multiple-upload-di-codeigniter.html', '1', 'Testing data');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(128) NOT NULL,
  `keterangan` text,
  `created_at` varchar(45) NOT NULL,
  `updated_at` varchar(45) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO users VALUES ('2', '1', '1', 'Admin', 'admin@gmail.com', '$2y$10$ekSgfgIEPOjEIdFbmPys0Oy9BJ8noJqrKMQSvKf5KTBe4K4rHBlQK', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO users VALUES ('3', '5', null, 'Putra', 'putra@gmail.com', '$2y$10$AKnI9KYhTxBbwAUc9JpnhOwF79JagZG2.vhxXcDXPneG56Z2p928u', null, '2020-07-11 21:56:32', '', '1');

-- ----------------------------
-- Table structure for `user_access_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_access_role`;
CREATE TABLE `user_access_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `user_access_role_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `user_access_role_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=731 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_access_role
-- ----------------------------
INSERT INTO user_access_role VALUES ('719', '1', '30');
INSERT INTO user_access_role VALUES ('720', '1', '31');
INSERT INTO user_access_role VALUES ('721', '1', '34');
INSERT INTO user_access_role VALUES ('722', '1', '44');
INSERT INTO user_access_role VALUES ('723', '1', '69');
INSERT INTO user_access_role VALUES ('724', '1', '77');
INSERT INTO user_access_role VALUES ('725', '1', '78');
INSERT INTO user_access_role VALUES ('726', '1', '79');
INSERT INTO user_access_role VALUES ('727', '5', '69');
INSERT INTO user_access_role VALUES ('728', '5', '77');
INSERT INTO user_access_role VALUES ('729', '5', '78');
INSERT INTO user_access_role VALUES ('730', '5', '79');
