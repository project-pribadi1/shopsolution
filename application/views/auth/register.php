<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Register - Shopsolution</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/img/icon/icon-logo.png') ?>">
    <link href="<?= base_url() ?>assets/dist/css/styles.css" rel="stylesheet" />
    <script src="<?= base_url() ?>assets/dist/js/all.min.js" crossorigin="anonymous"></script>
</head>

<style>
    :root {
        --input-padding-x: 1.5rem;
        --input-padding-y: 0.75rem;
    }

    .login,
    .image {
        min-height: 100vh;
    }

    .bg-image {
        background-image: url('https://source.unsplash.com/WEQbe2jBg40/600x1200');
        background-size: cover;
        background-position: center;
    }

    .login-heading {
        font-weight: 300;
    }

    .btn-login {
        font-size: 0.9rem;
        letter-spacing: 0.05rem;
        padding: 0.75rem 1rem;
        border-radius: 2rem;
    }

    .form-label-group {
        position: relative;
        margin-bottom: 1rem;
    }

    .form-label-group>input,
    .form-label-group>label {
        padding: var(--input-padding-y) var(--input-padding-x);
        height: auto;
        border-radius: 2rem;
    }

    .form-label-group>label {
        position: absolute;
        top: 0;
        left: 0;
        display: block;
        width: 100%;
        margin-bottom: 0;
        /* Override default `<label>` margin */
        line-height: 1.5;
        color: #495057;
        cursor: text;
        /* Match the input under the label */
        border: 1px solid transparent;
        border-radius: .25rem;
        transition: all .1s ease-in-out;
    }

    .form-label-group input::-webkit-input-placeholder {
        color: transparent;
    }

    .form-label-group input:-ms-input-placeholder {
        color: transparent;
    }

    .form-label-group input::-ms-input-placeholder {
        color: transparent;
    }

    .form-label-group input::-moz-placeholder {
        color: transparent;
    }

    .form-label-group input::placeholder {
        color: transparent;
    }

    .form-label-group input:not(:placeholder-shown) {
        padding-top: calc(var(--input-padding-y) + var(--input-padding-y) * (2 / 3));
        padding-bottom: calc(var(--input-padding-y) / 3);
    }

    .form-label-group input:not(:placeholder-shown)~label {
        padding-top: calc(var(--input-padding-y) / 3);
        padding-bottom: calc(var(--input-padding-y) / 3);
        font-size: 12px;
        color: #777;
    }

    /* Fallback for Edge
    -------------------------------------------------- */

    @supports (-ms-ime-align: auto) {
        .form-label-group>label {
            display: none;
        }

        .form-label-group input::-ms-input-placeholder {
            color: #777;
        }
    }

    /* Fallback for IE
    -------------------------------------------------- */

    @media all and (-ms-high-contrast: none),
    (-ms-high-contrast: active) {
        .form-label-group>label {
            display: none;
        }

        .form-label-group input:-ms-input-placeholder {
            color: #777;
        }
    }
</style>

<body>
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
            <div class="col-md-8 col-lg-6">
                <div class="login d-flex align-items-center py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-lg-10 mx-auto">
                                <img class="text-center" src="<?= base_url('assets/img/icon/icon-logo-0.png') ?>" width="220px" height="220px">

                                <h5 class="login-heading mb-4">Register your account</h5>
                                <?= $this->session->flashdata('message'); ?>
                                <form method="POST" action="<?= base_url('auth/register') ?>" enctype="multipart/form-data">
                                    <div class="form-label-group">
                                        <input type="text" id="inputFullname" name="full_name" class="form-control" placeholder="Full Name" value="<?= set_value('full_name') ?>" required autofocus>
                                        <label for="inputFullname">Full Name</label>
                                        <?= form_error('full_name', '<small class="text-danger">', '</small><br>'); ?>
                                    </div>
                                    <div class="form-label-group">

                                        <input type="text" id="inputTelp" name="telpon" class="form-control" value="<?= set_value('telpon') ?>" placeholder="Telp" required>
                                        <label for="inputTelp">Whatsapp</label>

                                        <figcaption class="figure-caption text-red mt-1 pl-2">Example image : 62835314118</figcaption>
                                        <?= form_error('telpon', '<small class="text-danger">', '</small><br>'); ?>

                                    </div>

                                    <div class="form-label-group">
                                        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" value="<?= set_value('email') ?>" required>
                                        <label for="inputEmail">Email address</label>
                                        <?= form_error('email', '<small class="text-danger">', '</small><br>'); ?>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-label-group">
                                                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
                                                <label for="inputPassword">Password</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-label-group">
                                                <input type="password" id="inputPassword2" name="confirm_password" class="form-control" placeholder="Password" required>
                                                <label for="inputPassword2">Confirm Password</label>
                                            </div>
                                        </div>
                                        <?= form_error('confirm_password', '<small class="text-danger">', '</small><br>'); ?>
                                    </div>


                                    <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Register</button>
                                    <a href="<?= base_url('auth') ?>" class="btn btn-lg btn-default btn-block btn-login text-uppercase font-weight-bold mb-2">Login</a>
                                    <div class="text-center">
                                        <a class="small" href="#">Return home</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?= base_url() ?>assets/dist/js/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/js/scripts.js"></script>
</body>

</html>