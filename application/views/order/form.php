<main>
    <div class="container-fluid">

        <h1 class="mt-4">Order</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Form</li>
        </ol>
        <?= $this->session->flashdata('message'); ?>

        <div class="card mb-4">
            <div class="card-header">
                Form Order
            </div>
            <form action="<?= base_url('order/create') ?>" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="jumlah_form" id="jumlah_form" value="1">
                <div class="card-body">
                    <div class="row" id="form-template">
                        <div class="col-md-6 mt-3" class="form-order" id="form-order-0">
                            <div class="card">
                                <div class="card-header">
                                    <button type="button" class="btn btn-sm float-right"><i class="fa fa-times" aria-hidden="true"></i></button>
                                </div>
                                <div class="card-body">
                                    <div class="col-md-12 col-md-6">
                                        <div class="form-group">
                                            <label for="">Foto</label>
                                            <br> <input type="file" id="image-0" name="image-0[]" multiple required>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Link</label>
                                            <textarea name="link-0" id="link-0" class="form-control" rows="2" required></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Quantity</label>
                                            <input type="number" name="quantity-0" id="quantity-0" class="form-control" value="1" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Description</label>
                                            <textarea name="description-0" id="description-0" class="form-control" rows="2" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <button onclick="addItem()" type="button" id="addForm" class="btn btn-sm btn-info float-right"><i class="fas fa-fw fa-plus"></i>Form</button>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="form-group">
                        <a href="<?= base_url('order/list') ?>" class="btn btn-danger btn-sm float-left">Cancel</a>
                        <button class="btn btn-primary btn-sm float-right" type="submit">Submit</button>
                    </div>
                    <br>
                </div>
            </form>
        </div>
    </div>
</main>
<script>
    var formNumber = 1;

    function addItem() {
        var parent = document.getElementById('form-template');
        var jumlahForm = document.getElementById('jumlah_form');
        var newChild = `
                        <div class="col-md-6 mt-3" class="form-order" id="form-order-${formNumber}">
                            <div class="card">
                                <div class="card-header">
                                    <button class="btn btn-sm float-right" type="button" onclick="deleteForm(${formNumber})"><i class="fa fa-times" aria-hidden="true"></i></button>
                                </div>
                                <div class="card-body">
                                    <div class="col-md-12 col-md-6">
                                        <div class="form-group">
                                            <label for="">Foto</label>
                                            <br> <input type="file" name="image-${formNumber}[]" id="image-${formNumber}" multiple required>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Link</label>
                                            <textarea name="link-${formNumber}" id="link-${formNumber}" class="form-control" rows="2" required></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Quantity</label>
                                            <input type="number" name="quantity-${formNumber}" id="quantity-${formNumber}" class="form-control" value="1" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Description</label>
                                            <textarea name="description-${formNumber}" id="description-${formNumber}" class="form-control" rows="2" required></textarea>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    `;
        parent.insertAdjacentHTML('beforeend', newChild);
        formNumber++;
        jumlahForm.value = formNumber;
    }

    function deleteForm(id) {
        var myobj = document.getElementById(`form-order-${id}`);
        myobj.remove();
    }
</script>