<main>
    <div class="container-fluid">

        <h1 class="mt-4">Order</h1>
        <!-- <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Form</li>
        </ol> -->
        <?= $this->session->flashdata('message'); ?>

        <div class="card mb-4 col-md-6 ">
            <div class="card-header">
                Edit Form
            </div>
            <form action="<?= base_url('order/update') ?>" method="POST" enctype="multipart/form-data">
                <input type="text" name="id" value="<?= $order->id ?>" hidden>
                <div class="card-body">
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Image</label>
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <?php $images = json_decode($order->image); ?>
                                <?php for ($i = 0; $i < sizeof($images); $i++) { ?>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i ?>" class="<?= ($i == 0) ? 'active' : '' ?>"></li>
                                <?php } ?>
                            </ol>
                            <div class="carousel-inner">
                                <?php for ($i = 0; $i < sizeof($images); $i++) { ?>
                                    <div class="carousel-item <?= ($i == 0) ? 'active' : '' ?>">
                                        <img class="d-block w-100" src="<?= base_url('assets/img/image-order/') . $images[$i] ?>">
                                    </div>
                                <?php } ?>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                        <br> <input type="file" id="image" name="image[]" multiple>
                        <figcaption class="figure-caption text-red" style="color: red">empty if you don't want to update image</figcaption>
                    </div>

                    <div class="form-group">
                        <label for="" class="font-weight-bold">Description</label>
                        <textarea name="description" id="description" class="form-control" rows="2" required><?= $order->description ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Link</label>
                        <textarea name="link" id="link" class="form-control" rows="2" required><?= $order->link ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-weight-bold">Quantity</label>
                        <input type="number" name="quantity" id="quantity" class="form-control" value="<?= $order->quantity ?>" required>
                    </div>
                    <!-- <div class="form-group">
                        <label for="" class="font-weight-bold">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="0" <?= (0 == $order->status) ? 'selected' : '' ?>>Cancel</option>
                            <option value="1" <?= (1 == $order->status) ? 'selected' : '' ?>>Order</option>
                            <option value="2" <?= (2 == $order->status) ? 'selected' : '' ?>>Process</option>
                            <option value="3" <?= (3 == $order->status) ? 'selected' : '' ?>>Done</option>
                        </select>
                    </div> -->
                </div>
                <div class="card-footer">
                    <div class="form-group ">
                        <a href="<?= base_url('member/listOrder') ?>" class="btn btn-danger btn-sm float-left">Cancel</a>
                        <button class="btn btn-primary btn-sm float-right" type="submit">Update</button>
                    </div>
                    <br>
                </div>
            </form>
        </div>
    </div>
</main>
<script>

</script>