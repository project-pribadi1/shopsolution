<?php
if (isset($_REQUEST["file"])) {
    // Get parameters
    $file = urldecode($_REQUEST["file"]); // Decode URL-encoded string

    /* Test whether the file name contains illegal characters
    such as "../" using the regular expression */
    if (preg_match('/^[^.][-a-z0-9_.]+[a-z]$/i', $file)) {
        $filepath = "images/" . $file;

        // Process download
        if (file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
            die();
        } else {
            http_response_code(404);
            die();
        }
    } else {
        die("Invalid file name!");
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Report Order</title>
</head>
<style>
    body {
        font-family: droidsansfallback, sans-serif;
    }

    @font-face {
        font-family: 'Firefly Sung';
        font-style: normal;
        font-size: 12pt;
        src: url(http://eclecticgeek.com/dompdf/fonts/cjk/fireflysung.ttf) format('truetype');
    }

    .chinese {
        /* font-family: Firefly Sung; */
        font-family: Firefly;
    }

    .wrapper {
        width: 850px;
        margin: auto;
        border: 2px solid #444;
        padding: 20px;
    }

    #konvert {
        background: #444;
        border: 2px solid #444;
        border-radius: 1px;
        padding: 10px;
        color: #fff;
        font-weight: bold;
    }

    #konvert:hover {
        background: #fff;
        color: #444;
    }
</style>

<body>
    <div>
        <h2>Order : <?= $order[0]->member . ' / ' . $order[0]->telpon ?></h2>
        <h3>Invoice : <?= $order[0]->invoice ?></h3>
        <!-- <h3>Invoice : <?= $order[0]->invoice ?></h3> -->
        <hr>
        <?php foreach ($order as $key => $value) { ?>
            <p><b>Item <?= $key + 1 ?></b></p>
            <p><b>Link :</b> <a href="<?= $value->link ?>"><?= $value->link ?></a></p>
            <p><b>Quantity : </b> <?= $value->quantity ?></p>
            <p><b> Description :</b> <?= $value->description ?></p>
            <p><b>Image :</b></p>
            <br><br>
            <p>
                <?php $images = json_decode($value->image); ?>
                <?php for ($i = 0; $i < sizeof($images); $i++) { ?>
                    <img src="<?= FCPATH . 'assets/img/image-order/' . $images[$i] ?>" style="margin:5px;" height="100%" width="100%" alt="">
                <?php } ?>
            </p>
        <?php } ?>
    </div>
</body>

</html>