<?php $this->load->view('member/header'); ?>
<style>
    #hero {
        width: 100%;
        height: 100vh;
        background: url(<?= base_url('assets/img/austin-distel-Imc-IoZDMXc-unsplash.jpg') ?>) top center;
        background-size: cover;
        position: relative;
    }
</style>
<section id="hero" class="d-flex align-items-center justify-content-center">
    <div class="container text-left" data-aos="fade-up">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1>Welcome <?= $this->session->userdata('name'); ?></h1>

                <p class="lead">
                    <b>Impor Satuan CHN-INDO</b>
                    <br style="font-style: italic;"> Spesialis MIX TOKO China’s Marketplace
                </p>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>

</section>
<!-- End Hero -->

<!-- ======= About Section ======= -->
<section id="about" class="about">
    <div class="container" data-aos="fade-up">
        <div class="row m-2" data-aos="fade-up">
            <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
                <img src="https://images.unsplash.com/photo-1556155092-490a1ba16284?ixlib=rb-1.2.1&auto=format&fit=crop&w=1500&q=80" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content" data-aos="fade-right" data-aos-delay="100">
                <h3>HOW WE WORK ??</h3>
                <ul>
                    <li><i class="ri-check-double-line"></i> Register your account.</li>
                    <li><i class="ri-check-double-line"></i> Logging on website.</li>
                    <li><i class="ri-check-double-line"></i> Add your order.</li>
                    <li><i class="ri-check-double-line"></i> Fill the form.</li>
                    <li><i class="ri-check-double-line"></i> Submit.</li>
                    <li><i class="ri-check-double-line"></i> We will contact you soon.</li>
                </ul>
                <p>
                    <a href="<?= base_url('auth/register') ?>" class="btn btn-sm mt-3 text-white" style="background-color: #f4623a !important;">Register Now</a>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End About Section -->

<!-- ======= Testimonials Section ======= -->
<section style="background-color: #f4623a !important;">
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-md-12 justify-content-center">
                <h3 class="text-center mb-3 text-white">Testimonial</h3>
            </div>
            <div class="col-sm-4 m-0" data-aos="fade-up-left">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Zackly</h5>
                        <p class="card-text" style="font-style: italic;">"Recomended import."</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 m-0" data-aos="fade-up-left">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Zackly</h5>
                        <p class="card-text" style="font-style: italic;">"Recomended import."</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 m-0" data-aos="fade-up-left">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Zackly</h5>
                        <p class="card-text" style="font-style: italic;">"Recomended import."</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Testimonials Section -->

<section class="page-section bg-white" id="contact">
    <div class="container" data-aos="fade-up">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h2 class="mt-0">Contact US!</h2>
                <hr class="divider my-4" />
                <!-- <p class="text-muted mb-5">Ready to start your next project with us? Give us a call or send us an
                    email and we will get back to you as soon as possible!</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
                <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
                <div><?= WHATSAPP ?></div>
            </div>
            <div class="col-lg-4 mr-auto text-center">
                <i class="fas fa-envelope fa-3x mb-3 text-muted"></i>
                <!-- Make sure to change the email address in BOTH the anchor text and the link target below!-->
                <a class="d-block" href="mailto:contact@yourwebsite.com"><?= EMAIL ?></a>
            </div>
        </div>
    </div>
</section>
<script src="<?= base_url('assets/owl.carousel/owl.carousel.min.js') ?> "></script>

<!-- WhatsHelp.io widget -->
<script type="text/javascript">

    function aos_init() {
        AOS.init({
            duration: 1000,
            easing: "ease-in-out",
            once: true,
            mirror: false
        });
    }

    // Testimonials carousel (uses the Owl Carousel library)
    $(".testimonials-carousel").owlCarousel({
        autoplay: true,
        dots: true,
        loop: true,
        items: 1
    });

    $(window).on('load', function() {
        aos_init();
    });

    $('#dataTable').dataTable({
        "columnDefs": [{
            "width": "200px",
            'target': 1
        }]
    });
    (function() {
        var options = {
            call: "<?= TELPON ?>", // Call phone number
            whatsapp: "<?= WHATSAPP ?>",
            greeting_message: "Welcome to shopsolution", // Text of greeting message
            call_to_action: "Chat With Us", // Call to action
            button_color: "#000000", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,line,call,email, whatsapp" // Order of buttons
        };
        var proto = "https:",
            host = "whatshelp.io",
            url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function() {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /GetButton.io widget -->
<?php $this->load->view('member/footer'); ?>