<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>/assets/img/icon/icon-logo.png">
    <link rel="icon" type="image/x-icon" href="<?= base_url() ?>/assets/img/icon/icon-logo.png">
    <!-- untuk  penulis dari website-->
    <meta name="author" content="Shopsolutioncn">
    <!-- untuk memberikan description pada search enggine -->
    <meta name="keywords" content="Jastip, Jastip Produk China, Shopsolution, Jasa Titip Barang, Spesialis Multi Toko">
    <!-- untuk keyword yang akan di cari search enggine -->
    <meta name="description" content="Jasa titip barang produk china, spesialis multi toko harga tetap sama">

    <meta property="og:title" content="Shopsolution | Jastip Produk Chinas">
    <meta property="og:description" content="Jasa titip barang produk china, spesialis multi toko harga tetap sama">
    <meta property="og:image" content="<?= base_url() ?>/assets/img/icon/icon-logo.png">
    <meta property="og:url" content="http://shopsolutioncn.com/">
    <meta property="og:type" content="website" />

    <meta name="twitter:card" content="summary_large_image">

    <!--  Non-Essential, But Recommended -->

    <meta property="og:site_name" content="Shopsolution.cn.">
    <meta name="twitter:image:alt" content="Jastip Produk Chinas">


    <!--  Non-Essential, But Required for Analytics -->

    <meta property="fb:app_id" content="your_app_id" />
    <meta name="twitter:site" content="@shopsolution.cn">
    <title>Shopsolution | Jastip Produk Chinas</title>


    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/asset/css/bootstrap.min.css">

    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/asset/css/font-awesome.css"> -->
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/fontawesome.min.css">

    <link rel="stylesheet" href="<?= base_url() ?>/asset/css/templatemo-breezed.css">

    <link rel="stylesheet" href="<?= base_url() ?>/asset/css/owl-carousel.css">

    <link rel="stylesheet" href="<?= base_url() ?>/asset/css/lightbox.css">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <style>
        body {
            font-family: 'Montserrat', sans-serif;
        }

        .timeline {
            list-style: none;
            padding: 20px 0 20px;
            position: relative;
        }

        .timeline:before {
            top: 0;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 3px;
            background-color: #eeeeee;
            left: 50%;
            margin-left: -1.5px;
        }

        .timeline>li {
            margin-bottom: 20px;
            position: relative;
        }

        .timeline>li:before,
        .timeline>li:after {
            content: " ";
            display: table;
        }

        .timeline>li:after {
            clear: both;
        }

        .timeline>li:before,
        .timeline>li:after {
            content: " ";
            display: table;
        }

        .timeline>li:after {
            clear: both;
        }

        .timeline>li>.timeline-panel {
            width: 30%;
            float: left;
            border: 1px solid #d4d4d4;
            border-radius: 5px;
            padding: 20px;
            position: relative;
            -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
            box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        }

        .timeline>li>.timeline-panel:before {
            position: absolute;
            top: 26px;
            right: -15px;
            display: inline-block;
            border-top: 15px solid transparent;
            border-left: 15px solid #ccc;
            border-right: 0 solid #ccc;
            border-bottom: 15px solid transparent;
            content: " ";
        }

        .timeline>li>.timeline-panel:after {
            position: absolute;
            top: 27px;
            right: -14px;
            display: inline-block;
            border-top: 14px solid transparent;
            border-left: 14px solid #fff;
            border-right: 0 solid #fff;
            border-bottom: 14px solid transparent;
            content: " ";
        }

        .timeline>li>.timeline-badge {
            color: #fff;
            width: 50px;
            height: 50px;
            line-height: 50px;
            font-size: 1.4em;
            text-align: center;
            position: absolute;
            top: 16px;
            left: 50%;
            margin-left: -25px;
            background-color: #999999;
            z-index: 100;
            border-top-right-radius: 50%;
            border-top-left-radius: 50%;
            border-bottom-right-radius: 50%;
            border-bottom-left-radius: 50%;
        }

        .timeline>li.timeline-inverted>.timeline-panel {
            float: right;
        }

        .timeline>li.timeline-inverted>.timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        .timeline>li.timeline-inverted>.timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }

        .timeline-badge.primary {
            background-color: #2e6da4 !important;
        }

        .timeline-badge.success {
            background-color: #3f903f !important;
        }

        .timeline-badge.warning {
            background-color: #f0ad4e !important;
        }

        .timeline-badge.danger {
            background-color: #d9534f !important;
        }

        .timeline-badge.info {
            background-color: #5bc0de !important;
        }

        .timeline-title {
            margin-top: 0;
            color: inherit;
        }

        .timeline-body>p,
        .timeline-body>ul {
            margin-bottom: 0;
        }

        .timeline-body>p+p {
            margin-top: 5px;
        }

        #taobao {
            margin-left: 60%;
        }

        #h3-taobao {
            margin-left: 70%;
        }

        #www-1688 {
            margin-left: 60%;
        }

        #h3-1688 {
            margin-left: 75%;
        }

        @media (max-width: 767px) {
            ul.timeline:before {
                left: 40px;
            }

            ul.timeline>li>.timeline-panel {
                width: calc(100% - 90px);
                width: -moz-calc(100% - 90px);
                width: -webkit-calc(100% - 90px);
            }

            ul.timeline>li>.timeline-badge {
                left: 15px;
                margin-left: 0;
                top: 16px;
            }

            ul.timeline>li>.timeline-panel {
                float: right;
            }

            ul.timeline>li>.timeline-panel:before {
                border-left-width: 0;
                border-right-width: 15px;
                left: -15px;
                right: auto;
            }

            ul.timeline>li>.timeline-panel:after {
                border-left-width: 0;
                border-right-width: 14px;
                left: -14px;
                right: auto;
            }

            .card {
                margin-top: 3px;
            }

            .sponsor-3 {
                margin-top: 20px;
            }

            .kelebihan {
                margin-top: 10px;
            }

            #taobao {
                margin-left: 30%;
            }

            #h3-taobao {
                margin-left: 40%;
            }

            #www-1688 {
                margin-left: 30%;
            }

            #h3-1688 {
                margin-left: 50%;
            }
        }
    </style>
</head>

<body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->


    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="index.html" class="logo">
                            <a href="<?= base_url('home') ?>"><img src="<?= base_url('assets/img/icon/icon-logo-0.png') ?>" height="90px;" alt="Indico"></a>
                            <!-- .BREEZED -->
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li><a href="<?= base_url('home') ?>" class="active">Home</a></li>
                            <li><a href="<?= base_url('tracking') ?>">Tracking</a></li>
                            <li><a href="#contact-us">Contact Us</a></li>
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Main Banner Area Start ***** -->
    <div class="main-banner header-text" id="top">
        <div class="Modern-Slider">
            <!-- Item -->
            <div class="item">
                <div class="img-fill">
                    <img src="<?= base_url() ?>/asset/images/slide/slide-new-1.jpg" style="opacity:60%;" alt="">
                    <div class="text-content" data-aos="zoom-in" data-aos-duration="1000" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
                        <h3><a href="https://shopsolutioncn.com/" class="text-white">shopsolutioncn.com</a></h3>
                        <h5 class="mb-0" style="font-weight: 600;">We help you buy <br> from chinese e-commerces.</h5>
                        <p class="text-white p-0">
                            We accepted taobao, 1688, TMall and other chinese platform.
                        </p>
                    </div>
                </div>
            </div>
            <!-- // Item -->
            <!-- Item -->
            <div class="item">
                <div class="img-fill">
                    <img src="<?= base_url() ?>/asset/images/slide/slide-new-2.jpg" style="opacity:60%;" alt="">
                    <div class="text-content text-left">
                        <h3><a href="https://shopsolutioncn.com/" class="text-white">shopsolutioncn.com</a></h3>
                        <h5 class="mb-0" style="font-weight: 600;">We provide multi store goods <br> in one shipping cost.</h5>
                        <p class="text-white p-0">
                            We have storage in china to provide effecient and cheap shipping cost.
                        </p>
                    </div>
                </div>
            </div>
            <!-- // Item -->
            <!-- Item -->
            <div class="item">
                <div class="img-fill">
                    <img src="<?= base_url() ?>/asset/images/slide/slide-new-3.jpg" style="opacity:60%;" alt="">
                    <div class="text-content text-right">
                        <h3><a href="https://shopsolutioncn.com/" class="text-white">shopsolutioncn.com</a></h3>
                        <h5 class="mb-0" style="font-weight: 600;">We are committed to providing <br> transparent prices.</h5>
                        <p class="text-white p-0">
                            We provide real price in platform without adding any price.
                        </p>
                    </div>
                </div>
            </div>
            <!-- // Item -->
        </div>
    </div>
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** About Area Starts ***** -->
    <!-- <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="left-text-content">
                        <div class="section-heading">
                            <h6>About Us</h6>
                            <h2>We work with top brands and startups</h2>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="service-item">
                                    <img src="<?= base_url() ?>/asset/images/service-item-01.png" alt="">
                                    <h4>Top Notch</h4>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="service-item">
                                    <img src="<?= base_url() ?>/asset/images/service-item-01.png" alt="">
                                    <h4>Robust</h4>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="service-item">
                                    <img src="<?= base_url() ?>/asset/images/contact-info-03.png" alt="">
                                    <h4>Reliable</h4>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="service-item">
                                    <img src="<?= base_url() ?>/asset/images/contact-info-03.png" alt="">
                                    <h4>Up-to-date</h4>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <a href="#" class="main-button-icon">
                                    Learn More <i class="fa fa-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="right-text-content">
                        <p><a rel="nofollow noopener" href="https://templatemo.com/tm-543-breezed" target="_parent">Breezed HTML Template</a> is provided by TemplateMo for absolutely free
                            of charge. You can download, edit and use this for your non-commercial and commercial
                            websites.
                            <br><br>Redistributing this template as a downloadable ZIP file on any template collection
                            website is strictly prohibited. You will need to talk to us if you want to redistribute this
                            template. Thank you.
                            <br><br>This is a Bootstrap v4.3.1 CSS layout. Do you like it? You can feel free to <a rel="nofollow noopener" href="https://templatemo.com/contact" target="_parent">talk to
                                us</a> if you have anything.</p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- ***** About Area Ends ***** -->
    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <div class="features-item text-center">
                        <div style="color: #ff4200; font-size: 60px; margin-bottom: 10px;">
                            <i class="fas fa-dollar-sign"></i>0
                        </div>
                        <h3>No Fee</h3>
                        <p>No extra fee exclude shipping cost.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <div class="features-item text-center">
                        <div style="color: #ff4200; font-size: 60px; margin-bottom: 10px;">
                            <i class="fas fa-glasses"></i>
                        </div>
                        <h3>Transparent</h3>
                        <p>Pay the price shown in apps. We do not mark up the price.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <div class="features-item text-center">
                        <div style="color: #ff4200; font-size: 60px; margin-bottom: 10px;">
                            <i class="far fa-handshake"></i>
                        </div>
                        <h3>Trusted</h3>
                        <p>Satisfied 300+ happy customer with 5000+ processed items.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->

    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="subscribe" style="background-color: #ff4200">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="section-heading">
                        <!-- <h6>Tracking you order</h6> -->
                        <h2>Tracking you order</h2>
                    </div>
                    <div class="subscribe-content">
                        <div class="subscribe-form">
                            <form id="subscribe-now" action="<?= base_url('home/tracking') ?>" method="GET" id="form-resi">
                                <div class="row">
                                    <div class="col-md-8 col-sm-12">
                                        <fieldset>
                                            <input name="resi" type="text" id="resi" placeholder="Enter your resi..." required="">
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <fieldset>
                                            <button type="submit" id="form-submit" class="main-button">Tracking</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->


    <!-- ***** Projects Area Starts ***** -->
    <section class="section" id="projects">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 sm-padding text-center mb-3" data-aos="zoom-in" data-aos-duration="1000">
                    <div class="about-content wow">
                        <h2>Why <br><strong style="color: #ff4200 ">shopsolution ??</strong> </h2>
                    </div>
                </div>
                <div class="col-md-6 " data-aos="zoom-in" data-aos-duration="1000" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
                    <div class="card" style="border-radius: 15px;">
                        <div class="card-body">
                            <h3 class="text-center" style="color: #ff4200;"><strong>SHOPSOLUTION.CN</strong></h3>
                            <div class="row text-center">
                                <div class="col-md-4">
                                    <strong>TOKO A</strong>
                                    <div class="info-box" style="color: #ff4200;">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.25 KG</small>
                                </div>
                                <div class="col-md-4">
                                    <strong>TOKO A</strong>
                                    <div class="info-box" style="color: #ff4200;">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.5 KG</small>
                                </div>
                                <div class="col-md-4">
                                    <strong>TOKO A</strong>
                                    <div class="info-box" style="color: #ff4200;">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.25 KG</small>
                                </div>
                                <hr>
                                <div class="col-md-12 text-center">
                                    <br>
                                    <p style="color: black; margin-bottom: 0px;">1 KG</p>
                                    <p style="color: black; margin-top:1px; font-size:30px;"><strong>40.000</strong></p>
                                    <h5 style="color: #ff4200; font-size:20px;">BEBAS CAMPUR TOKO</h5>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" data-aos="zoom-in" data-aos-duration="1000" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                    <div class="card" style="border-radius: 15px;">
                        <div class="card-body">
                            <h3 class="text-center"><strong>JASTIP</strong></h3>
                            <div class="row text-center">
                                <div class="col-md-4">
                                    <strong>TOKO A</strong>
                                    <div class="info-box">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.25 KG</small>
                                </div>
                                <div class="col-md-4">
                                    <strong>TOKO A</strong>
                                    <div class="info-box">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.5 KG</small>
                                </div>
                                <div class="col-md-4">
                                    <strong>TOKO A</strong>
                                    <div class="info-box">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.25 KG</small>
                                </div>
                                <hr>
                                <div class="col-md-12 text-center">
                                    <br>
                                    <p style="color: black; margin-bottom: 0px;">3 KG</p>
                                    <p style="color: black; margin-top:1px; font-size:30px;"><strong>120.000</strong></p>
                                    <h5 style="color: #ff4200; font-size:20px;">MINIMAL 1 KG / TOKO</h5>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- ***** Projects Area Ends ***** -->

    <!-- ***** Testimonials Starts ***** -->
    <section class="section" style="margin-top: 100px;" id="how-to-work">
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="text-center">
                        <h1 id="timeline" data-aos="zoom-in" data-aos-duration="500" style="color: #ff4200; font-weight:bold;">How to Work ?</h1>
                    </div>
                    <ul class="timeline">
                        <li>
                            <div class="timeline-badge" data-aos-delay="500" data-aos-offset="0"><i class="fab fa-whatsapp"></i></div>
                            <div class="timeline-panel" data-aos="zoom-in" data-aos-duration="1000" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title" style="color:black; font-size:14px;">Send link & image</h4>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-badge warning"><i class="fa fa-credit-card"></i></div>
                            <div class="timeline-panel" data-aos="zoom-in" data-aos-duration="1100" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title" style="color:black; font-size:14px;">Payments</h4>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-badge danger"><i class="fa fa-file-invoice-dollar"></i></div>
                            <div class="timeline-panel" data-aos="zoom-in" data-aos-duration="1200" data-aos-easing="ease-in-back" data-aos-delay="400" data-aos-offset="0">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title" style="color:black; font-size:14px;">Receive receipt</h4>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-badge info"><i class="fa fa-bookmark"></i></div>
                            <div class="timeline-panel" data-aos="zoom-in" data-aos-duration="1300" data-aos-easing="ease-in-back" data-aos-delay="500" data-aos-offset="0">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title" style="color:black; font-size:14px;">Done and wait</h4>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-badge success"><i class="fa fa-check"></i></div>
                            <div class="timeline-panel" data-aos="zoom-in" data-aos-duration="1400" data-aos-easing="ease-in-back" data-aos-delay="600" data-aos-offset="0">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title" style="color:black; font-size:14px;">Receive and enjoy the goods</h4>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>

    <section class="section" id="testimonials" style="background-color: black; margin-top:100px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h6>Digital Team</h6>
                        <h2>young and talented members</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ***** Contact Us Area Starts ***** -->
    <section class="section" id="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="left-text-content">
                        <div class="section-heading">
                            <h6>Contact Us</h6>
                            <h2>Feel free to keep in touch with us!</h2>
                        </div>
                        <ul class="contact-info">
                            <li><img src="<?= base_url() ?>/asset/images/contact-info-01.png" alt="">010-020-0860</li>
                            <li><img src="<?= base_url() ?>/asset/images/contact-info-02.png" alt="">info@company.com</li>
                            <li><img src="<?= base_url() ?>/asset/images/contact-info-03.png" alt="">www.company.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-xs-12">
                    <div class="contact-form">
                        <form id="contact" action="" method="get">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <fieldset>
                                        <input name="name" type="text" id="name" placeholder="Your Name *" required="">
                                    </fieldset>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <fieldset>
                                        <input name="phone" type="text" id="phone" placeholder="Your Phone" required="">
                                    </fieldset>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <fieldset>
                                        <input name="email" type="email" id="email" placeholder="Your Email *" required="">
                                    </fieldset>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <fieldset>
                                        <input name="subject" type="text" id="subject" placeholder="Subject">
                                    </fieldset>
                                </div>
                                <div class="col-lg-12">
                                    <fieldset>
                                        <textarea name="message" rows="6" id="message" placeholder="Message" required=""></textarea>
                                    </fieldset>
                                </div>
                                <div class="col-lg-12">
                                    <fieldset>
                                        <button type="submit" id="form-submit" class="main-button-icon">Send Message Now
                                            <i class="fa fa-arrow-right"></i></button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Contact Us Area Ends ***** -->
    <div class="scroll-down scroll-to-section"><a href="#top" class="float-right"><i class="fa fa-arrow-up"></i></a></div>

    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <div class="left-text-content">
                        <p>Copyright &copy; 2020 Breezed Co., Ltd.

                            - Design: <a rel="nofollow noopener" href="https://templatemo.com">TemplateMo</a></p>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="right-text-content">
                        <ul class="social-icons">
                            <li>
                                <p>Follow Us</p>
                            </li>
                            <li><a rel="nofollow" href="https://fb.com/templatemo"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a rel="nofollow" href="https://fb.com/templatemo"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a rel="nofollow" href="https://fb.com/templatemo"><i class="fa fa-linkedin"></i></a>
                            </li>
                            <li><a rel="nofollow" href="https://fb.com/templatemo"><i class="fa fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="<?= base_url() ?>asset/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="<?= base_url() ?>/asset/js/popper.js"></script>
    <script src="<?= base_url() ?>/asset/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="<?= base_url() ?>/asset/js/owl-carousel.js"></script>
    <script src="<?= base_url() ?>/asset/js/scrollreveal.min.js"></script>
    <script src="<?= base_url() ?>/asset/js/waypoints.min.js"></script>
    <script src="<?= base_url() ?>/asset/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url() ?>/asset/js/imgfix.min.js"></script>
    <script src="<?= base_url() ?>/asset/js/slick.js"></script>
    <script src="<?= base_url() ?>/asset/js/lightbox.js"></script>
    <script src="<?= base_url() ?>/asset/js/isotope.js"></script>

    <!-- Global Init -->
    <script src="<?= base_url() ?>/asset/js/custom.js"></script>

    <script>
        $(function() {
            $('.slick-arrow').remove();
            AOS.init();
            var selectedClass = "";
            $("p").click(function() {
                selectedClass = $(this).attr("data-rel");
                $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("." + selectedClass).fadeOut();
                setTimeout(function() {
                    $("." + selectedClass).fadeIn();
                    $("#portfolio").fadeTo(50, 1);
                }, 500);

            });
        });
    </script>

</body>

</html>