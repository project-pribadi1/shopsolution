<!doctype html>

<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/img/icon/icon-logo.png') ?>">
    <link rel="icon" type="image/x-icon" href="<?= base_url('assets/img/icon/icon-logo.png') ?>">
    <!-- untuk  penulis dari website-->
    <meta name="author" content="Shopsolution">
    <!-- untuk memberikan description pada search enggine -->
    <meta name="keywords" content="Jastip, Jastip Produk China, Shopsolution, Jasa Titip Barang, Spesialis Multi Toko">
    <!-- untuk keyword yang akan di cari search enggine -->
    <meta name="description" content="Jasa titip barang produk china, spesialis multi toko harga tetap sama">

    <meta property="og:title" content="Shopsolution | Jastip Produk Chinas">
    <meta property="og:description" content="Jasa titip barang produk china, spesialis multi toko harga tetap sama">
    <meta property="og:image" content="<?= base_url('assets/img/icon/icon-logo.png') ?>">
    <meta property="og:url" content="http://shopsolutioncn.com/">
    <meta property="og:type" content="website" />

    <meta name="twitter:card" content="summary_large_image">

    <!--  Non-Essential, But Recommended -->

    <meta property="og:site_name" content="Shopsolution.cn.">
    <meta name="twitter:image:alt" content="Jastip Produk Chinas">


    <!--  Non-Essential, But Required for Analytics -->

    <meta property="fb:app_id" content="your_app_id" />
    <meta name="twitter:site" content="@shopsolution.cn">
    <title>Shopsolution | Jastip Produk Chinas</title>

    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/fontawesome.min.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/themify-icons.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/elegant-line-icons.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/elegant-font-icons.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/flaticon.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/animate.min.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/slick.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/slider.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/odometer.min.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/venobox/venobox.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/main.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/responsive.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

</head>

<body>
    <div class="site-preloader-wrap">
        <div class="spinner"></div>
    </div>

    <header class="header" id="header">
        <nav class="primary-header">
            <div class="container">
                <div class="primary-header-inner">
                    <div class="header-logo">
                        <a href="<?= base_url('home') ?>"><img src="<?= base_url('assets/img/icon/icon-logo-0.png') ?>" height="90px;" alt="Indico"></a>
                    </div>
                    <div class="header-menu-wrap">
                        <ul class="dl-menu">
                            <li><a href="<?= base_url('home') ?>">Home</a></li>
                            <li><a href="<?= base_url('home/tracking') ?>" class="active">Tracking</a></li>
                            <li><a href="#contact">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="header-right">
                        <div class="mobile-menu-icon">
                            <div class="burger-menu">
                                <div class="line-menu line-half first-line"></div>
                                <div class="line-menu"></div>
                                <div class="line-menu line-half last-line"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <section class="about-section padding" id="tracking">
        <div class="container">
            <div class="row about-wrap">
                <div class="col-md-12 text-center" data-aos="zoom-in" data-aos-duration="1000">
                    <h2>Tracking Pesananmu</h2>
                    <p>Silakan input kode pesanan untuk mencari tahun progress pesananan mu.</p>
                </div>
                <div class="col-lg-12 col-sm-6 sm-padding mt-3" data-aos="zoom-in" data-aos-duration="1500">
                    <div class="widget-content">
                        <div class="subscribe-form-wrap col-md-6">
                            <form action="#" id="form-resi" class="subscribe-form">
                                <input type="text" name="search" id="resi" class="form-input" placeholder="Enter Your no order or invoice or resi" required>
                                <button type="button" onclick="search_order()" class="submit-btn">Track</button>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="col-lg-12 sm-padding mt-3" data-aos="zoom-in" data-aos-duration="2000">
                    <div class="table-responsive">
                        <table class="table table-sm table-bordered table-stripped" style="color: black;" id="table-tracking">
                            <thead>
                                <tr class="text-center text-nowrap" style="color: #263a4f">
                                    <th width="50px">#</th>
                                    <th width="100px">No Order</th>
                                    <th>Invoice</th>
                                    <th>Resi</th>
                                    <th>Date</th>
                                    <th>China's Port</th>
                                    <th>Local Arrived</th>
                                    <th>Purchased</th>
                                    <th>Picked up</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($hasil) {
                                    foreach ($hasil as $key => $value) { ?>
                                        <tr>
                                            <td class="text-center"><?= $key + 1 ?></td>
                                            <td><?= $value->no ?></td>
                                            <td><?= $value->invoice ?></td>
                                            <td><?= $value->resi ?></td>
                                            <td><?= ($value->date) ? $value->date : '-' ?></td>
                                            <td><?= ($value->chinas_port) ? $value->chinas_port : '-' ?></td>
                                            <td><?= ($value->local_arrived) ? $value->local_arrived : '-' ?></td>
                                            <td class="text-center"><?= $value->purchased ?></td>
                                            <td class="text-center"><?= $value->picked_up ?></td>
                                        </tr>
                                    <?php }
                                } else { ?>
                                    <tr>
                                        <td colspan="9" class="text-center">Data not found</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="widget-section sm-padding" id="contact" style="background-color: #ff4200">
        <div class="container">
            <div class="row mt-5">
                <div class="col-lg-6 col-sm-6 mt-5">
                    <div class="widget-content">
                        <a href="www.shopsolutioncn.com"><img src="<?= base_url('assets/img/icon/logo-putih.png') ?>" width="150px" alt="brand"></a>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 mt-5">
                    <div class="widget-content">
                        <h4>Contact Us</h4>
                        <ul class="widget-links ">
                            <li><a href="wa.me/<?= WHATSAPP ?>" class="text-white"> <i class="fab fa-whatsapp"></i> Whatsapp</a></li>
                            <li><a href="https://instagram.com/shopsolution.cn?igshid=1qqtxgxfgr6vz" class="text-white"> <i class="fab fa-instagram"></i> Instagram</a></li>
                            <li><a href="https://www.shopsolutioncn.com/" class="text-white"> <i class="fab fa-chrome"></i> www.shopsolutioncn.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <a data-scroll href="#header" id="scroll-to-top"><i class="arrow_carrot-up"></i></a>

    <script src="<?= base_url('assets_home/') ?>js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/bootstrap.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/tether.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/headroom.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/owl.carousel.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/smooth-scroll.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/venobox.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/jquery.ajaxchimp.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/slick.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/waypoints.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/odometer.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/wow.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/main.js"></script>
    <script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2@9.js') ?>"></script>
    <script>
        $(document).ready(function() {
            AOS.init();
            (function() {
                var options = {
                    whatsapp: "<?= WHATSAPP ?>", // WhatsApp number
                    call_to_action: "Message us", // Call to action
                    position: "left", // Position may be 'right' or 'left'
                };
                // var proto = document.location.protocol,
                var proto = 'https:',
                    host = "getbutton.io",
                    url = proto + "//static." + host;
                var s = document.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = url + '/widget-send-button/js/init.js';
                s.onload = function() {
                    WhWidgetSendButton.init(host, proto, options);
                };
                var x = document.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            })();
        });

        function search_order() {
            $.ajax({
                type: 'POST',
                url: "<?= base_url() ?>home/get_order",
                data: $('#form-resi').serialize(),
                dataType: "json",
                beforeSend: function() {
                    alert_loading()
                },
                success: function(result) {
                    $('tbody tr').remove()
                    if (result.length > 0) {
                        result.forEach(function(order, index) {
                            loadtable = `
                                            <tr>
                                                <td class="text-center">${index+1}</td>
                                                <td>${order.no}</td>
                                                <td>${order.invoice}</td>
                                                <td>${order.resi}</td>
                                                <td>${(order.date) ? order.date : '-' }</td>
                                                <td>${(order.chinas_port) ? order.chinas_port : '-' }</td>
                                                <td>${(order.local_arrived) ? order.local_arrived : '-' }</td>
                                                <td>${order.purchased}</td>
                                                <td>${order.picked_up}</td>
                                            </tr>
                                        `;
                            $('#table-tracking tbody').append(loadtable);
                            if (index + 1 == result.length) {
                                Swal.close();
                            }
                        });

                    } else {
                        loadtable = `
                                        <tr>
                                            <td colspan="9" class="text-center">Data not found</td>
                                        </tr>
                                    `;
                        $('#table-tracking tbody').append(loadtable);
                        Swal.close();
                    }
                },
                error: function(error) {
                    alert('error')
                },
            });
        }



        function alert_loading() {
            let timerInterval
            Swal.fire({
                title: 'waiting!',
                html: 'Search order, please wait!!.',
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
            })
        }
    </script>
</body>

</html>