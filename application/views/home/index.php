<!doctype html>

<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/img/icon/icon-logo.png') ?>">
    <link rel="icon" type="image/x-icon" href="<?= base_url('assets/img/icon/icon-logo.png') ?>">
    <!-- untuk  penulis dari website-->
    <meta name="author" content="Shopsolution">
    <!-- untuk memberikan description pada search enggine -->
    <meta name="keywords" content="Jastip, Jastip Produk China, Shopsolution, Jasa Titip Barang, Spesialis Multi Toko">
    <!-- untuk keyword yang akan di cari search enggine -->
    <meta name="description" content="Jasa titip barang produk china, spesialis multi toko harga tetap sama">

    <meta property="og:title" content="Shopsolution | Jastip Produk Chinas">
    <meta property="og:description" content="Jasa titip barang produk china, spesialis multi toko harga tetap sama">
    <meta property="og:image" content="<?= base_url('assets/img/icon/icon-logo.png') ?>">
    <meta property="og:url" content="http://shopsolutioncn.com/">
    <meta property="og:type" content="website" />

    <meta name="twitter:card" content="summary_large_image">

    <!--  Non-Essential, But Recommended -->

    <meta property="og:site_name" content="Shopsolution.cn.">
    <meta name="twitter:image:alt" content="Jastip Produk Chinas">


    <!--  Non-Essential, But Required for Analytics -->

    <meta property="fb:app_id" content="your_app_id" />
    <meta name="twitter:site" content="@shopsolution.cn">
    <title>Shopsolution | Jastip Produk Chinas</title>

    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/fontawesome.min.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/themify-icons.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/elegant-line-icons.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/elegant-font-icons.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/flaticon.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/animate.min.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/slick.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/slider.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/odometer.min.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/venobox/venobox.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/main.css">
    <link rel="stylesheet" href="<?= base_url('assets_home/') ?>css/responsive.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?= base_url() ?>/assets_home/css/templatemo-breezed.css">


    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <style>
        body {
            font-family: 'Montserrat', sans-serif;
        }

        .timeline {
            list-style: none;
            padding: 20px 0 20px;
            position: relative;
        }

        .timeline:before {
            top: 0;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 3px;
            background-color: #eeeeee;
            left: 50%;
            margin-left: -1.5px;
        }

        .timeline>li {
            margin-bottom: 20px;
            position: relative;
        }

        .timeline>li:before,
        .timeline>li:after {
            content: " ";
            display: table;
        }

        .timeline>li:after {
            clear: both;
        }

        .timeline>li:before,
        .timeline>li:after {
            content: " ";
            display: table;
        }

        .timeline>li:after {
            clear: both;
        }

        .timeline>li>.timeline-panel {
            width: 30%;
            float: left;
            border: 1px solid #d4d4d4;
            border-radius: 5px;
            padding: 20px;
            position: relative;
            -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
            box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        }

        .timeline>li>.timeline-panel:before {
            position: absolute;
            top: 26px;
            right: -15px;
            display: inline-block;
            border-top: 15px solid transparent;
            border-left: 15px solid #ccc;
            border-right: 0 solid #ccc;
            border-bottom: 15px solid transparent;
            content: " ";
        }

        .timeline>li>.timeline-panel:after {
            position: absolute;
            top: 27px;
            right: -14px;
            display: inline-block;
            border-top: 14px solid transparent;
            border-left: 14px solid #fff;
            border-right: 0 solid #fff;
            border-bottom: 14px solid transparent;
            content: " ";
        }

        .timeline>li>.timeline-badge {
            color: #fff;
            width: 50px;
            height: 50px;
            line-height: 50px;
            font-size: 1.4em;
            text-align: center;
            position: absolute;
            top: 16px;
            left: 50%;
            margin-left: -25px;
            background-color: #999999;
            z-index: 100;
            border-top-right-radius: 50%;
            border-top-left-radius: 50%;
            border-bottom-right-radius: 50%;
            border-bottom-left-radius: 50%;
        }

        .timeline>li.timeline-inverted>.timeline-panel {
            float: right;
        }

        .timeline>li.timeline-inverted>.timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        .timeline>li.timeline-inverted>.timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }

        .timeline-badge.primary {
            background-color: #2e6da4 !important;
        }

        .timeline-badge.success {
            background-color: #3f903f !important;
        }

        .timeline-badge.warning {
            background-color: #f0ad4e !important;
        }

        .timeline-badge.danger {
            background-color: #d9534f !important;
        }

        .timeline-badge.info {
            background-color: #5bc0de !important;
        }

        .timeline-title {
            margin-top: 0;
            color: inherit;
        }

        .timeline-body>p,
        .timeline-body>ul {
            margin-bottom: 0;
        }

        .timeline-body>p+p {
            margin-top: 5px;
        }

        #taobao {
            margin-left: 60%;
        }

        #h3-taobao {
            margin-left: 70%;
        }

        #www-1688 {
            margin-left: 60%;
        }

        #h3-1688 {
            margin-left: 75%;
        }

        @media (max-width: 767px) {
            ul.timeline:before {
                left: 40px;
            }

            ul.timeline>li>.timeline-panel {
                width: calc(100% - 90px);
                width: -moz-calc(100% - 90px);
                width: -webkit-calc(100% - 90px);
            }

            ul.timeline>li>.timeline-badge {
                left: 15px;
                margin-left: 0;
                top: 16px;
            }

            ul.timeline>li>.timeline-panel {
                float: right;
            }

            ul.timeline>li>.timeline-panel:before {
                border-left-width: 0;
                border-right-width: 15px;
                left: -15px;
                right: auto;
            }

            ul.timeline>li>.timeline-panel:after {
                border-left-width: 0;
                border-right-width: 14px;
                left: -14px;
                right: auto;
            }

            .card {
                margin-top: 3px;
            }

            .sponsor-3 {
                margin-top: 20px;
            }

            .kelebihan {
                margin-top: 10px;
            }

            #taobao {
                margin-left: 30%;
            }

            #h3-taobao {
                margin-left: 40%;
            }

            #www-1688 {
                margin-left: 30%;
            }

            #h3-1688 {
                margin-left: 50%;
            }

            #img-logo {
                margin-left: 0px;
            }
        }
    </style>
</head>

<body>
    <div class="site-preloader-wrap">
        <div class="spinner"></div>
    </div>

    <section id="header"></section>
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="<?= base_url('home') ?>" class="logo">
                            <img id="img-logo" src="<?= base_url('assets/img/icon/icon-logo-0.png') ?>" height="90px;" alt="Indico">
                        </a>
                        <!-- .BREEZED -->

                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li><a href="<?= base_url('home') ?>" class="active text-center">Home</a></li>
                            <li><a href="<?= base_url('home/tracking') ?>" class="text-center">Tracking</a></li>
                            <li><a href="#contact" class="text-center">Contact Us</a></li>
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->


    <!-- ***** Main Banner Area Start ***** -->
    <div class="main-banner header-text">
        <div class="Modern-Slider">
            <!-- Item -->
            <div class="item">
                <div class="img-fill">
                    <img src="<?= base_url('assets_home/') ?>img/slide-new-1.jpg" style="opacity:60%;" alt="">
                    <div class="text-content text-center">
                        <h3><a href="https://shopsolutioncn.com/" class="text-white">shopsolutioncn.com</a></h3>
                        <h5 class="mb-0" style="font-weight: 600;">We help you buy <br> from chinese e-commerces.</h5>
                        <p class="text-white p-0">
                            We accepted taobao, 1688, TMall and other chinese platform.
                        </p>
                    </div>
                </div>
            </div>
            <!-- // Item -->
            <!-- Item -->
            <div class="item">
                <div class="img-fill">
                    <img src="<?= base_url('assets_home/') ?>img/slide-new-2.jpg" style="opacity:60%;" alt="">
                    <div class="text-content text-left">
                        <h3><a href="https://shopsolutioncn.com/" class="text-white">shopsolutioncn.com</a></h3>
                        <h5 class="mb-0" style="font-weight: 600;">We provide multi store goods <br> in one shipping cost.</h5>
                        <p class="text-white p-0">
                            We have storage in china to provide effecient and cheap shipping cost.
                        </p>
                    </div>
                </div>
            </div>
            <!-- // Item -->
            <!-- Item -->
            <div class="item">
                <div class="img-fill">
                    <img src="<?= base_url('assets_home/') ?>img/slide-new-3.jpg" style="opacity:60%;" alt="">
                    <div class="text-content text-right">
                        <h3><a href="https://shopsolutioncn.com/" class="text-white">shopsolutioncn.com</a></h3>
                        <h5 class="mb-0" style="font-weight: 600;">We are committed to providing <br> transparent prices.</h5>
                        <p class="text-white p-0">
                            We provide real price in platform without adding any price.
                        </p>
                    </div>
                </div>
            </div>
            <!-- // Item -->
        </div>
    </div>
    <!-- ***** Main Banner Area End ***** -->

    <section class="about-section bg-grey padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4 kelebihan" data-aos="zoom-in" data-aos-duration="1000" data-aos-easing="ease-in-back" data-aos-delay="100" data-aos-offset="0">
                    <div class="service-item">
                        <div class="service-icon">
                            <i class="fas fa-dollar-sign"></i>0
                        </div>
                        <h3>No Fee</h3>
                        <p>No extra fee exclude shipping cost.</p>
                        <div class="overlay-icon">
                            <i class="fas fa-dollar-sign"></i>0
                        </div>
                    </div>
                </div>
                <div class="col-md-4 kelebihan" data-aos="zoom-in" data-aos-duration="1000" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
                    <div class="service-item">
                        <div class="service-icon">
                            <i class="fas fa-glasses"></i>
                        </div>
                        <h3>Transparent</h3>
                        <p>Pay the price shown in apps. We do not mark up the price.</p>
                        <div class="overlay-icon">
                            <i class="fas fa-glasses"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 kelebihan" data-aos="zoom-in" data-aos-duration="1000" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                    <div class="service-item">
                        <div class="service-icon">
                            <i class="far fa-handshake"></i>
                        </div>
                        <h3>Trusted</h3>
                        <p>Satisfied 300+ happy customer with 5000+ processed items.</p>
                        <div class="overlay-icon">
                            <i class="far fa-handshake"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about-section padding" id="tracking">
        <div class="container">
            <div class="row about-wrap">
                <div class="col-md-12 text-center" data-aos="zoom-in" data-aos-duration="1000">
                    <h2>Tracking Pesananmu</h2>
                    <p>Silakan input kode pesanan untuk mencari tahun progress pesanananmu.</p>
                </div>
                <div class="col-lg-12 col-sm-6 sm-padding mt-3" data-aos="zoom-in" data-aos-duration="1500">
                    <div class="widget-content">
                        <div class="subscribe-form-wrap col-md-6">
                            <form action="<?= base_url('home/tracking') ?>" method="GET" id="form-resi" class="subscribe-form">
                                <input type="text" name="search" id="resi" class="form-input" placeholder="Enter Your no order or invoice or resi" required>
                                <button type="submit" class="submit-btn">Track</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about-section bg-grey padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 sm-padding text-center" data-aos="zoom-in" data-aos-duration="1000">
                    <div class="about-content wow">
                        <h2>Why <br><strong style="color: #ff4200 ">shopsolution ??</strong> </h2>
                    </div>
                </div>
                <div class="col-md-6" data-aos="zoom-in" data-aos-duration="1000" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
                    <div class="card" style="border-radius: 15px;">
                        <div class="card-body">
                            <h3 class="text-center" style="color: #ff4200;"><strong>SHOPSOLUTION.CN</strong></h3>
                            <div class="row text-center">
                                <div class="col-md-4">
                                    <strong>TOKO A</strong>
                                    <div class="info-box" style="color: #ff4200;">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.25 KG</small>
                                </div>
                                <div class="col-md-4">
                                    <strong>TOKO B</strong>
                                    <div class="info-box" style="color: #ff4200;">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.5 KG</small>
                                </div>
                                <div class="col-md-4">
                                    <strong>TOKO C</strong>
                                    <div class="info-box" style="color: #ff4200;">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.25 KG</small>
                                </div>
                                <hr>
                                <div class="col-md-12 text-center">
                                    <br>
                                    <p style="color: black; margin-bottom: 0px;">1 KG</p>
                                    <p style="color: black; margin-top:1px; font-size:30px;"><strong>40.000</strong></p>
                                    <h5 style="color: #ff4200; font-size:20px;">BEBAS CAMPUR TOKO</h5>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" data-aos="zoom-in" data-aos-duration="1000" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                    <div class="card" style="border-radius: 15px;">
                        <div class="card-body">
                            <h3 class="text-center"><strong>JASTIP</strong></h3>
                            <div class="row text-center">
                                <div class="col-md-4">
                                    <strong>TOKO A</strong>
                                    <div class="info-box">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.25 KG</small>
                                </div>
                                <div class="col-md-4">
                                    <strong>TOKO B</strong>
                                    <div class="info-box">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.5 KG</small>
                                </div>
                                <div class="col-md-4">
                                    <strong>TOKO C</strong>
                                    <div class="info-box">
                                        <i class="fas fa-shopping-bag fa-5x"></i>
                                    </div>
                                    <small style="color:black;">0.25 KG</small>
                                </div>
                                <hr>
                                <div class="col-md-12 text-center">
                                    <br>
                                    <p style="color: black; margin-bottom: 0px;">3 KG</p>
                                    <p style="color: black; margin-top:1px; font-size:30px;"><strong>120.000</strong></p>
                                    <h5 style="color: #ff4200; font-size:20px;">MINIMAL 1 KG / TOKO</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="projects-section padding" id="how-to-work">
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="text-center">
                        <h1 id="timeline" data-aos="zoom-in" data-aos-duration="500" style="color: #ff4200; font-weight:bold;">How to Work ?</h1>
                    </div>
                    <ul class="timeline">
                        <li>
                            <div class="timeline-badge" data-aos-delay="500" data-aos-offset="0"><i class="fab fa-whatsapp"></i></div>
                            <div class="timeline-panel" data-aos="zoom-in" data-aos-duration="1000" data-aos-easing="ease-in-back" data-aos-delay="200" data-aos-offset="0">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title" style="color:black;">Send link & image</h4>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-badge warning"><i class="fa fa-credit-card"></i></div>
                            <div class="timeline-panel" data-aos="zoom-in" data-aos-duration="1100" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title" style="color:black;">Payments</h4>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-badge danger"><i class="fa fa-file-invoice-dollar"></i></div>
                            <div class="timeline-panel" data-aos="zoom-in" data-aos-duration="1200" data-aos-easing="ease-in-back" data-aos-delay="400" data-aos-offset="0">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title" style="color:black;">Receive receipt</h4>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-badge info"><i class="fa fa-bookmark"></i></div>
                            <div class="timeline-panel" data-aos="zoom-in" data-aos-duration="1300" data-aos-easing="ease-in-back" data-aos-delay="500" data-aos-offset="0">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title" style="color:black;">Done and wait</h4>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-badge success"><i class="fa fa-check"></i></div>
                            <div class="timeline-panel" data-aos="zoom-in" data-aos-duration="1400" data-aos-easing="ease-in-back" data-aos-delay="600" data-aos-offset="0">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title" style="color:black;">Receive and enjoy the goods</h4>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>

    <section class="testimonial-section bg-white padding" style="background-color:white;">
        <div class="dots"></div>
        <div class="container">
            <div class="section-heading text-center mb-40 wow fadeInUp" data-wow-delay="100ms">
                <h2>We Accepted</h2>
            </div>
            <div id="testimonial-carousel" class="testimonial-carousel box-shadow owl-carousel">
                <div class="testi-item1 d-flex align-items-center">
                    <div class="testi-content text-center">
                        <a href="https://www.1688.com/" class="text-center">
                            <img src="<?= base_url('assets_home/') ?>img/1688.png" id="www-1688" class="text-center" style="height: 180px; width:180px;" alt="sponsor">
                        </a>
                        <h3 id="h3-1688">www.1688.com</h3>
                    </div>
                </div>
                <div class="testi-item1 d-flex align-items-center">
                    <div class="testi-content text-center mt-3">
                        <a href="https://world.taobao.com/">
                            <img src="<?= base_url('assets_home/') ?>img/taobao.png" style="height: 180px; width:180px;" id="taobao" alt="sponsor">
                        </a>
                        <h3 id="h3-taobao">world.taobao.com</h3>
                    </div>
                </div>
                <div class="testi-item1 d-flex align-items-center">
                    <div class="testi-content text-center  mt-3">
                        <a href="https://www.tmall.com/">
                            <img src="<?= base_url('assets_home/') ?>img/logo-3.png" style="height: 180px; width:450px;" alt="sponsor">
                        </a>
                        <h3>www.tmall.com</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="widget-section sm-padding" id="contact" style="background-color: #ff4200">
        <div class="container">
            <div class="row mt-5">
                <div class="col-lg-6 col-sm-6 mt-5">
                    <div class="widget-content">
                        <a href="https://www.shopsolutioncn.com/"><img src="<?= base_url('assets/img/icon/logo-putih.png') ?>" width="150px" alt="brand"></a>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 mt-5">
                    <div class="widget-content">
                        <h4>Contact Us</h4>
                        <ul class="widget-links ">
                            <li><a href="https://wa.me/<?= WHATSAPP ?>" class="text-white"> <i class="fab fa-whatsapp"></i> Whatsapp</a></li>
                            <li><a href="https://instagram.com/shopsolution.cn?igshid=1qqtxgxfgr6vz" class="text-white"> <i class="fab fa-instagram"></i> Instagram</a></li>
                            <li><a href="https://www.shopsolutioncn.com/" class="text-white"> <i class="fab fa-chrome"></i> www.shopsolutioncn.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <a href="#header" id="scroll-to-top"><i class="arrow_carrot-up"></i></a>

    <script src="<?= base_url('assets_home/') ?>js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/bootstrap.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/tether.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/headroom.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/owl.carousel.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/smooth-scroll.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/venobox.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/jquery.ajaxchimp.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/slick.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/waypoints.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/odometer.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/vendor/wow.min.js"></script>
    <script src="<?= base_url('assets_home/') ?>js/main.js"></script>
    <script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2@9.js') ?>"></script>
    <!-- Global Init -->


    <!-- Plugins -->
    <script src="<?= base_url('asset/js/') ?>scrollreveal.min.js"></script>
    <script src="<?= base_url('asset/js/') ?>jquery.counterup.min.js"></script>
    <script src="<?= base_url('asset/js/') ?>imgfix.min.js"></script>
    <!-- <script src="<?= base_url('asset/js/') ?>slick.js"></script> -->
    <script src="<?= base_url('asset/js/') ?>lightbox.js"></script>
    <script src="<?= base_url('asset/js/') ?>isotope.js"></script>

    <!-- Global Init -->
    <script src="<?= base_url('asset/js/') ?>custom.js"></script>

    <script>
        $(document).ready(function() {
            $('.slick-arrow').remove();
            AOS.init();

            (function() {
                var options = {
                    whatsapp: "<?= WHATSAPP ?>", // WhatsApp number
                    call_to_action: "Message us", // Call to action
                    position: "left", // Position may be 'right' or 'left'
                };
                // var proto = document.location.protocol,
                var proto = 'https:',
                    host = "getbutton.io",
                    url = proto + "//static." + host;
                var s = document.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = url + '/widget-send-button/js/init.js';
                s.onload = function() {
                    WhWidgetSendButton.init(host, proto, options);
                };
                var x = document.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            })();


        });

        $("form").submit(function() {
            alert_loading()
        });

        function alert_loading() {
            let timerInterval
            Swal.fire({
                title: 'waiting!',
                html: 'Search order, please wait!!.',
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
            })
        }
    </script>

</body>

</html>