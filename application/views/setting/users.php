<main>
    <div class="container-fluid">
        <h1 class="mb-4 mt-3">Users</h1>
        <!-- <ol class="breadcrumb mt-4">
            <li class="breadcrumb-item active">Dashboard</li>
        </ol> -->
        <?= $this->session->flashdata('message'); ?>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Data Users
                <a href="<?= base_url('setting/create_user') ?>" class="btn btn-primary float-right btn-sm"><i class="fas fa-fw fa-plus"></i> Add Users</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Group Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($users as $key => $value) { ?>
                                <tr>
                                    <td class="text-center"><?= $key + 1 ?></td>
                                    <td><?= $value->name ?></td>
                                    <td><?= $value->email ?></td>
                                    <td><?= $value->group_name ?></td>
                                    <td class="text-center"><?= ($value->is_active == 1) ? '<label class="badge badge-success">Aktif</label>' : '<label class="badge badge-danger">Non aktif</label>' ?></td>
                                    <td class="text-right">
                                        <a href="#" onclick="delete_confirm(<?= $value->id ?>)"><i class="fas fa-fw fa-trash"></i></a>
                                        <a href="#" data-id="<?= $value->id ?>" data-toggle="modal" data-target="#modal-users" class="btn-edit"><i class="fas fa-fw fa-pencil-alt"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>


<script>
    $(document).ready(function() {
        $(".table").DataTable();
    });


    function delete_confirm() {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = "<?= base_url('setting/delete_user/') ?>" + id;
            }
        })
    }
</script>
<!-- /.content-wrapper -->