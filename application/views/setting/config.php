<main>
    <div class="container-fluid">
        <h1 class="mb-4 mt-3">Telegram</h1>
        <?= $this->session->flashdata('message'); ?>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Telegram Configuration
                <a href="#" class="btn btn-primary float-right btn-sm" data-toggle="modal" onclick="clear_form()" data-target="#modal-config"><i class="fas fa-fw fa-plus"></i> Add Config</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Api Key</th>
                                <th>Telegram Destination</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($config_telegram as $key => $value) { ?>
                                <tr>
                                    <td class="text-center"><?= $key + 1 ?></td>
                                    <td><?= $value->api_key ?></td>
                                    <td><?= $value->send_to ?></td>
                                    <td>
                                        <?php if ($value->status == 1) {
                                            echo "<label class='badge badge-success'>Active</label>";
                                        } else {
                                            echo "<label class='badge badge-danger'>Non Active</label>";
                                        } ?>
                                    </td>
                                    <td class="text-right">
                                        <a href="#" onclick="confirm_delete(<?= $value->id ?>)"><i class="fas fa-fw fa-trash"></i></a>
                                        <a href="#" data-id="<?= $value->id ?>" data-toggle="modal" data-target="#modal-config" class=" btn-edit"><i class="fas fa-fw fa-pencil-alt"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="modal-config">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Menu</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('setting/add_config') ?>" id="form-config" method="post" enctype="multipart/form-data">
                <input type="text" id="id" name="id" hidden>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Api Key</label>
                        <input type="text" name="api_key" id="api_key" class="form-control form-control-sm" placeholder="Api key">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">ID Destination</label>
                        <input type="text" name="send_to" id="send_to" class="form-control form-control-sm" placeholder="Destination id">
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" id="status" class="form-control form-control-sm select2">
                            <option default>Select Status</option>
                            <option value="1">Active</option>
                            <option value="0">Non Active</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    $(document).ready(function() {
        $(".table").DataTable();
    });

    function confirm_delete(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = "<?= base_url('setting/delete_config/') ?>" + id;
            }
        })
    }

    function clear_form() {
        $('#form-config')[0].reset();
    }

    $('.btn-edit').click(function() {
        const id = $(this).data('id');
        $.ajax({
            url: "<?= base_url() . 'setting/edit_config'; ?>",
            async: false,
            type: 'POST',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(data) {
                $('#id').val(data.id);
                $('#api_key').val(data.api_key);
                $('#send_to').val(data.send_to);
                $('#status').val(data.status);
            }
        });
    });
</script>
<!-- /.content-wrapper -->