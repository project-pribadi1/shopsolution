<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title><?= $title ?> - Shopsolution</title>
    <link rel="icon" href="fav/icon" type="image/gif" sizes="16x16">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/img/icon/icon-logo.png') ?>">


    <link href="<?= base_url() ?>assets/dist/css/styles.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/dist/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />


    <script src="<?= base_url() ?>assets/dist/js/all.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/js/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/assets/demo/datatables-demo.js"></script>

    <!-- select2 -->
    <link href="<?= base_url('assets/plugins/select2-3.5.3/select2.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/plugins/select2-3.5.3/select2-bootstrap.css') ?>" rel="stylesheet" />
    <script src="<?= base_url('assets/plugins/select2-3.5.3/select2.min.js') ?>"></script>

    <!-- sweetalert -->
    <script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2@9.js') ?>"></script>

</head>

<body class="sb-nav-fixed">
    <style>
        .navbar {
            font-weight: 700;
            font-family: "Merriweather Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        }
    </style>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
        <div class="container">
            <a class="navbar-brand" href="#">Shop Solution</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item <?= ($active == 'home') ? 'active' : '' ?>">
                        <a class="nav-link" href="<?= base_url('admin') ?>">Home </a>
                    </li>
                    <li class="nav-item <?= ($active == 'form') ? 'active' : '' ?>">
                        <a class="nav-link" href="<?= base_url('admin/form') ?>">Form </a>
                    </li>
                    <?php if ($this->session->userdata('email')) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= base_url('auth/logout') ?>">Logout &nbsp; <i class="fa fa-sign-out-alt"></i> </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </nav>