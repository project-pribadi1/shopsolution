<!-- Footer -->
<footer class="py-5 bg-dark">
  <div class="container">
    <p class="m-0 text-center text-white">Copyright &copy; <a href="<?= base_url('home') ?>">Shopsolution</a> 2020</p>
  </div>
  <!-- /.container -->
</footer>
</body>

</html>