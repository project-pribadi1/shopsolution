<?php $this->load->view('member/header'); ?>
<section class="pt-5">
    <div class="container">
        <div class="row pt-4">
            <div class="col-lg-8 mx-auto">
                <h4>Change Password</h4>
                <?= $this->session->flashdata('message'); ?>
                <form action="<?= base_url('member/changePassword') ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" id="id" value="<?= $this->session->userdata('id') ?>">
                    <div class="col-md-12 col-md-6">
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Current Password</label>
                            <input type="password" class="form-control" id="current_password" name="current_password" required>
                            <?= form_error('current_password', '<small class="text-danger">', '</small><br>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-weight-bold">New Password</label>
                            <input type="password" class="form-control" id="new_password" name="new_password" required>
                            <?= form_error('confirm_password', '<small class="text-danger">', '</small><br>'); ?>

                        </div>
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Confirm Password</label>
                            <input type="password" class="form-control" id="confirm_password" name="confirm_password" required>
                        </div>

                        <div class="form-group">
                            <button onclick="window.history.back(-1)" class="btn btn-danger btn-sm float-left">Cancel</button>
                            <button class="btn btn-primary btn-sm float-right" type="submit">Change Password</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('member/footer'); ?>