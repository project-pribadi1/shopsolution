<?php $this->load->view('member/header'); ?>
<section class="mt-0">
    <div class="container">
        <div class="row mt-4">
            <div class="col-lg-8 mx-auto">
                <h4>Form Order</h4>
                <input type="text" id="test" value="1" hidden>
                <form action="<?= base_url('member/createOrder') ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="jumlah_form" id="jumlah_form" value="1">
                    <!-- <center> -->
                    <div class="card-body justify-content-center align-items-center">
                        <div class="row" id="form-template">
                            <div class="col-md-12 col-md-6 mt-3" class="form-order" id="form-order-0">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="formclass">Form 1</h4>
                                        <button type="button" class="btn btn-sm float-right"></button>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-md-12 col-md-6">
                                            <div class="form-group">
                                                <label for="" class="font-weight-bold">Image</label>
                                                <br> <input type="file" id="image-0" name="image-0[]" multiple required>
                                                <figcaption class="figure-caption text-red mt-1" style="color: red">Example image : <a href="#" data-toggle="modal" data-target="#modal1">Click for priview</a></figcaption>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="font-weight-bold">Link</label>
                                                <textarea name="link-0" id="link-0" class="form-control" rows="2" required></textarea>
                                                <figcaption class="figure-caption text-red mt-1" style="color: red">Example Link : <a href="https://m.tb.cn/h.Vuqe15r?sm=20fea9">https://m.tb.cn/h.Vuqe15r?sm=20fea9</a></figcaption>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="font-weight-bold">Quantity</label>
                                                <input type="number" name="quantity-0" id="quantity-0" class="form-control" value="1" required>
                                            </div>
                                            <div class="form-group">
                                                <label for=""><span class="font-weight-bold">Description</span> <i>(size, color, etc)</i></label>
                                                <textarea name="description-0" id="description-0" class="form-control" rows="2" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <button onclick="addItem()" type="button" id="addForm" class="btn btn-sm btn-info float-right"><i class="fas fa-fw fa-plus"></i>Form</button>
                            </div>
                        </div>
                    </div>
                    <!-- </center> -->
                    <div class="card-footer">
                        <div class="form-group">
                            <a href="<?= base_url('order/list') ?>" class="btn btn-danger btn-sm float-left">Cancel</a>
                            <button class="btn btn-primary btn-sm float-right" type="submit">Submit</button>
                        </div>
                        <br>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--Body-->
            <div class="modal-body mb-0 p-0">
                <img class="modal-content" id="img01" src="<?= base_url('assets/img/example.jpeg') ?>">
            </div>
            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<?php $this->load->view('member/footer'); ?>

<script>
    var formNumber = 1;
    var formData = 1;
    var newChild = null;

    function addItem() {
        let test = document.getElementById('test');
        var parent = document.getElementById('form-template');
        var jumlahForm = document.getElementById('jumlah_form');
        if (test.value < 10) {
            test.value++;
            newChild = `
                        <div class="col-md-12 col-md-6 mt-3" class="form-order" id="form-order-${formNumber}">
                            <div class="card">
                                <div class="card-header">
                                    <button class="btn btn-sm float-right" type="button" onclick="deleteForm(${formNumber})"><i class="fa fa-times" aria-hidden="true"></i></button>
                                    <h4 class="formclass"></h4>
                                </div>
                                <div class="card-body">
                                    <div class="col-md-12 col-md-6">
                                        <div class="form-group">
                                            <label for="" class="font-weight-bold">Foto</label>
                                            <br> <input type="file" name="image-${formNumber}[]" id="image-${formNumber}" multiple required>
                                            <figcaption class="figure-caption text-red mt-1" style="color: red">Example image : <a href="#" data-toggle="modal" data-target="#modal1">Click for priview</a></figcaption>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="font-weight-bold">Link</label>
                                            <textarea name="link-${formNumber}" id="link-${formNumber}" class="form-control" rows="2" required></textarea>
                                            <figcaption class="figure-caption text-red mt-1" style="color: red">Example Link : <a href="https://m.tb.cn/h.Vuqe15r?sm=20fea9">https://m.tb.cn/h.Vuqe15r?sm=20fea9</a></figcaption>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="font-weight-bold">Quantity</label>
                                            <input type="number" name="quantity-${formNumber}" id="quantity-${formNumber}" class="form-control" value="1" required>
                                        </div>
                                        <div class="form-group">
                                            <label for=""><span class="font-weight-bold">Description</span> <i>(size, color, etc)</i></label>
                                            <textarea name="description-${formNumber}" id="description-${formNumber}" class="form-control" rows="2" required></textarea>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    `;
            parent.insertAdjacentHTML('beforeend', newChild);
            formNumber++;
            jumlahForm.value = formNumber;
            namaForm();
        } else {
            // formData -= 1;
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Cannot add more than 10 forms',
                // footer: '<a href>Why do I have this issue?</a>'
            })
        }

    }

    function deleteForm(id) {
        var myobj = document.getElementById(`form-order-${id}`);
        let test = document.getElementById('test');
        myobj.remove();
        test.value--;
        namaForm();
    }

    function namaForm() {
        const inputs = document.querySelectorAll('.formclass');

        inputs.forEach(function(input, index) {
            console.log(index);
            input.innerHTML = `Form ${index+1}`;
        });
    }

    
</script>