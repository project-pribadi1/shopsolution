<?php $this->load->view('member/header'); ?>
<section class="pt-5">
    <div class="container">
        <div class="row pt-4">
            <div class="col-lg-8 mx-auto">
                <h4>Edit Form</h4>
                <form action="<?= base_url('member/updateOrder') ?>" method="POST" enctype="multipart/form-data">
                    <div class="card mb-4 col-md-6">
                        <input type="text" name="id" value="<?= $order->id ?>" hidden>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="" class="font-weight-bold">Image</label>
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <?php $images = json_decode($order->image); ?>
                                        <?php for ($i = 0; $i < sizeof($images); $i++) { ?>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i ?>" class="<?= ($i == 0) ? 'active' : '' ?>"></li>
                                        <?php } ?>
                                    </ol>
                                    <div class="carousel-inner">
                                        <?php for ($i = 0; $i < sizeof($images); $i++) { ?>
                                            <div class="carousel-item <?= ($i == 0) ? 'active' : '' ?>">
                                                <img class="d-block w-100" src="<?= base_url('assets/img/image-order/') . $images[$i] ?>">
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>

                                <br> <input type="file" id="image" name="image[]" multiple>
                                <figcaption class="figure-caption text-red" style="color: red">empty if you don't want to update image</figcaption>
                            </div>

                            <div class="form-group">
                                <label for="" class="font-weight-bold">Description</label>
                                <textarea name="description" id="description" class="form-control" rows="2" required><?= $order->description ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="" class="font-weight-bold">Link</label>
                                <textarea name="link" id="link" class="form-control" rows="2" required><?= $order->link ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="" class="font-weight-bold">Quantity</label>
                                <input type="number" name="quantity" id="quantity" class="form-control" value="<?= $order->quantity ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="" class="font-weight-bold">Status</label>
                                <p>
                                    <?php
                                    if ($order->status == 1) {
                                        echo '<label class="badge badge-info">Order</label>';
                                    } elseif ($order->status == 2) {
                                        echo '<label class="badge badge-warning">Proses</label>';
                                    } elseif ($order->status == 3) {
                                        echo '<label class="badge badge-success">Done</label>';
                                    } else {
                                        echo '<label class="badge badge-danger">Cancel</label>';
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="form-group ">
                                <button onclick="window.history.back(-1)" class="btn btn-danger btn-sm float-left">Cancel</button>
                                <button class="btn btn-primary btn-sm float-right" type="submit">Update</button>
                            </div>
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('member/footer'); ?>