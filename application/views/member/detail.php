<?php $this->load->view('member/header'); ?>
<section class="pt-5">
    <div class="container">
        <div class="row pt-4">

            <div class="col-lg-8 mx-auto">
                <!-- <a href="<?= base_url('member/editOrder/') . $order->id ?>" class="float-right" title="Edit"><i class="fas fa-fw fa-pencil-alt"></i> Edit</a> -->

                <h4>Detail Order : <?= date('d F Y', strtotime($order[0]->date)) ?></h4>

                <div class="row">

                    <a href="#" onclick="window.history.back(-1)" class="col-md-6 float-left" style="color: red;"><i class="fas fa-undo"></i> Back</a>
                    <a target="_blank" class="col-md-6 float-right" href="<?= base_url('member/download/') . $order[0]->id ?>"><i class="fas fa-download"></i> Download</a>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->session->flashdata('message'); ?>
                    </div>
                    <?php foreach ($order as $key => $value) { ?>
                        <div class="card col-md-5" style="margin: 10px">
                            <div class="card-header">
                                Detail
                                <a href="<?= base_url('member/editOrder/') . $value->item_id ?>" class="float-right"><i class="fas fa-fw fa-pencil-alt"></i> Edit</a>
                            </div>
                            <div class="card-body">
                                <h5>Images</h5>
                                <div id="carouselExampleIndicators-<?= $key ?>" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <?php $images = json_decode($value->image); ?>
                                        <?php for ($i = 0; $i < sizeof($images); $i++) { ?>
                                            <li data-target="#carouselExampleIndicators-<?= $key ?>" data-slide-to="<?= $i ?>" class="<?= ($i == 0) ? 'active' : '' ?>"></li>
                                        <?php } ?>
                                    </ol>
                                    <div class="carousel-inner">
                                        <?php for ($i = 0; $i < sizeof($images); $i++) { ?>
                                            <div class="carousel-item <?= ($i == 0) ? 'active' : '' ?>">
                                                <img class="d-block w-100" src="<?= base_url('assets/img/image-order/') . $images[$i] ?>">
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-<?= $key ?>" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-<?= $key ?>" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>

                                <!-- <label for=""></label> -->
                                <div class="form-group mt-3">
                                    <label for="" class="font-weight-bold">Description</label>
                                    <p><?= $value->description ?></p>
                                </div>
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">Quanitity</label>
                                    <p><?= $value->quantity ?></p>
                                </div>
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">Link</label>
                                    <p><a href="<?= $value->link ?>"><?= $value->link ?></a></p>
                                </div>
                                <!-- <div class="form-group">
                                <label for="" class="font-weight-bold">Status</label>
                                <p>
                                    <?php
                                    if ($value->status == 1) {
                                        echo '<label class="badge badge-info">Order</label>';
                                    } elseif ($value->status == 2) {
                                        echo '<label class="badge badge-warning">Proses</label>';
                                    } elseif ($value->status == 3) {
                                        echo '<label class="badge badge-success">Done</label>';
                                    } else {
                                        echo '<label class="badge badge-danger">Cancel</label>';
                                    }
                                    ?>
                                </p>
                            </div> -->
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <hr>
                <div class="form-group">
                    <!-- <a href="<?= base_url('member/editOrder/') . $order->id ?>" class="float-right" title="Edit"><i class="fas fa-fw fa-pencil-alt"></i> Edit</a> -->
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('member/footer'); ?>
<script>
    $(document).ready(function() {
        $('.carousel').carousel();
    })
</script>
<!-- /.content-wrapper -->