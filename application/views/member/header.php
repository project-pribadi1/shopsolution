<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/img/icon/icon-logo.png') ?>">
    <title><?= ($title) ? $title . ' - ' : '' ?> Shopsolution</title>

    <link href="<?= base_url() ?>assets/dist/css/styles.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/dist/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />

    <link href="<?= base_url() ?>assets/dist/css/scrolling-nav.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <script src="<?= base_url() ?>assets/dist/js/all.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/js/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/dist/assets/demo/datatables-demo.js"></script>

    <script src="<?= base_url() ?>assets/dist/js/scrolling-nav.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <!-- sweetalert -->
    <script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2@9.js') ?>"></script>
</head>

<body id="page-top">

    <!-- Navigation -->
    <nav style="background-color: #f4623a !important;" class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="<?= base_url('assets/img/icon/logo-putih.png') ?>" width="60px" height="60px" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">Home</a>
                    </li>
                    <?php if ($this->session->userdata('email')) { ?>
                        <!-- <li class="nav-item">
                            <a class="nav-link text-white" href="<?= base_url('member') ?>">Beranda</a>
                        </li> -->
                        <li class="nav-item">
                            <a class="nav-link text-white" href="<?= base_url('member/listOrder') ?>">Order</a>
                        </li>

                        <li class="nav-item dropdown text-white">
                            <a class="nav-link dropdown-toggle text-white" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i> Profile </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="<?= base_url('member/changePassword') ?>">Change Password</a>
                                <!-- <a class="dropdown-item" href="#">Activity Log</a> -->
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?= base_url('auth/logout') ?>">Logout</a>
                            </div>
                        </li>
                    <?php } else { ?>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="<?= base_url('auth') ?>"><i class="fas fa-sign-in-alt" aria-hidden="true"></i> Login</a>
                        </li>
                    <?php } ?>
                </ul>

            </div>
        </div>
    </nav>