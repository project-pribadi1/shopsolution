<?php $this->load->view('member/header'); ?>
<section class="mt-0">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Beranda</li>
        </ol>
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <a href="<?= base_url('member/formOrder') ?>" class="btn btn-info btn-md float-right mb-3">Add Order</a>
                <h4>New Order</h4>
                <div class="table-responsive ">
                    <table class="table table-bordered table-striped nowraper" id="dataTable" width="100%">
                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Link</th>
                                <th>Description</th>
                                <th>Quantity</th>
                                <th>status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($orders as $key => $value) { ?>
                                <tr>
                                    <td class="text-center"><?= $key + 1 ?></td>
                                    <td style="width: 100px;"><a href="<?= $value->link ?>">Link reference</a></td>
                                    <td><?= $value->description ?></td>
                                    <td class="text-center"><?= $value->quantity ?></td>
                                    <td class="text-center">
                                        <?php
                                        if ($value->status == 1) {
                                            echo '<label class="badge badge-info">Order</label>';
                                        } elseif ($value->status == 2) {
                                            echo '<label class="badge badge-warning">Proses</label>';
                                        } elseif ($value->status == 3) {
                                            echo '<label class="badge badge-success">Done</label>';
                                        } else {
                                            echo '<label class="badge badge-danger">Cancel</label>';
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if ($value->status == 1) { ?>
                                            <a href="#" onclick="confirm_delete(<?= $value->id ?>)" title="Delete"><i class="fas fa-fw fa-trash"></i></a>
                                            <a href="<?= base_url('member/editOrder/') . $value->id ?>" title="Edit"><i class="fas fa-fw fa-pencil-alt"></i></a>
                                        <?php } ?>
                                        <a href="<?= base_url('member/detailOrder/') . $value->id ?>" title="Detail"><i class="far fa-eye"></i></a>
                                        <a href="<?= base_url('member/download/') . $value->id ?>" title="Dwonload"><i class="fas fa-download"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    $('#dataTable').dataTable({
        "columnDefs": [{
            "width": "200px",
            'target': 1
        }]
    });
    (function() {
        var options = {
            call: "<?= TELPON ?>", // Call phone number
            whatsapp: "<?= WHATSAPP ?>",
            greeting_message: "Welcome to shopsolution", // Text of greeting message
            call_to_action: "Chat With Us", // Call to action
            button_color: "#000000", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,line,call,email, whatsapp" // Order of buttons
        };
        var proto = "https:",
            host = "whatshelp.io",
            url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function() {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /GetButton.io widget -->
<?php $this->load->view('member/footer'); ?>