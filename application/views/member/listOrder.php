<?php $this->load->view('member/header'); ?>
<section class="mt-0">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Order</li>
        </ol>
        <div class="row pt-4">
            <div class="col-lg-12 mx-auto">
                <?= $this->session->flashdata('message'); ?>
                <a href="<?= base_url('member/formOrder') ?>" class="btn btn-info btn-md float-right mb-3">Add Order</a>
                <h4>All Order</h4>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Invoice</th>
                                <th>Date Order</th>
                                <th>status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($orders as $key => $value) { ?>
                                <tr>
                                    <td class="text-center"><?= $key + 1 ?></td>
                                    <td><?= $value->invoice ?></td>
                                    <td><?= date('d F Y H:i:s', strtotime($value->created_at)) ?></td>
                                    <td class="text-center">
                                        <?php
                                        if ($value->status == 1) {
                                            echo '<label class="badge badge-warning">PENDING</label>';
                                        } elseif ($value->status == 2) {
                                            echo '<label class="badge badge-info">APPROVE</label>';
                                        } elseif ($value->status == 3) {
                                            echo '<label class="badge badge-success">DONE</label>';
                                        } else {
                                            echo '<label class="badge badge-danger">CANCEL</label>';
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if ($value->status == 1) { ?>
                                            <a href="#" onclick="confirm_delete(<?= $value->id ?>)" title="Delete"><i class="fas fa-fw fa-trash"></i></a>
                                        <?php } ?>
                                        <a href="<?= base_url('member/detailOrder/') . $value->id ?>" title="Detail"><i class="far fa-eye"></i></a>
                                        <a target="_blank" href="<?= base_url('member/download/') . $value->id ?>" title="Dwonload"><i class="fas fa-download"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-order" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">How to order ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>
                    <li><i class="ri-check-double-line"></i> Click add order.</li>
                    <li><i class="ri-check-double-line"></i> Fill the form.</li>
                    <li><i class="ri-check-double-line"></i> Submit.</li>
                    <li><i class="ri-check-double-line"></i> We will contact you soon.</li>
                </ul>
                <hr class="devider">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="modal-show" value="1" id="defaultCheck1">
                            <label class="form-check-label" for="defaultCheck1">
                                Don't show again
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-secondary btn-sm float-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('member/footer'); ?>
<?php
$data = $this->db->get_where('users', ['id' => $this->session->userdata('id')])->row();
?>
<script>
    $('.form-check-input').on('click', function() {
        let modalShow = 1;
        if ($(this).prop("checked") == true) {
            modalShow = 0;
        } else if ($(this).prop("checked") == false) {
            modalShow = 1;
        }
        $.ajax({
            url: "<?= base_url('auth/changeModalShow') ?>",
            type: "post",
            data: {
                modalShow: modalShow,
            },
            success: function() {

            }
        })
    })
    $(document).ready(function() {
        $(".table").DataTable();
        const modal_show = `<?= $data->modal_show ?>`;
        console.log(modal_show);
        if (modal_show == 1) {
            $('#modal-order').modal('show');
        }
    });

    function confirm_delete(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = `<?= base_url('member/deleteOrder/') ?>${id}`;
            }
        })
    }

    (function() {
        var options = {
            // call: "<?= TELPON ?>", // Call phone number
            whatsapp: "<?= WHATSAPP ?>",
            greeting_message: "Welcome to shopsolution", // Text of greeting message
            call_to_action: "Chat With Us", // Call to action
            button_color: "#000000", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,line,call,email, whatsapp" // Order of buttons
        };
        var proto = "https:",
            host = "whatshelp.io",
            url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function() {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /.content-wrapper -->