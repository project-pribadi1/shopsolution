<main>
    <div class="container-fluid p-5">

        <h3>Form</h3>
        <hr>
        <?= $this->session->flashdata('message'); ?>
        <form action="<?= base_url('admin/create') ?>" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive-sm">
                        <table class="table table-hover table-sm table-striped table-bordered" id="table-form-order">
                            <thead>
                                <tr class="text-center text-nowrap">
                                    <th>Invoice</th>
                                    <th>Order No</th>
                                    <th>Resi</th>
                                    <th width="100px">China's Port</th>
                                    <th width="100px">Local Arrived</th>
                                    <th width="100px">Date</th>
                                    <th>Purchased</th>
                                    <th>Picked up</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="form-order-0">
                                    <td>
                                        <input type="text" class="form-control" name="invoice[]" id="invoice-0">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="no_order[]" id="no_order-0">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="resi[]" id="resi-0">
                                    </td>
                                    <td width="100px">
                                        <input type="date" class="form-control" name="chinas_port[]" id="china_port-0">
                                    </td>
                                    <td width="100px">
                                        <input type="date" class="form-control" name="local_arrived[]" id="local_arrived-0">
                                    </td>
                                    <td width="100px">
                                        <input type="date" class="form-control" name="date[]" id="date-0">
                                    </td>
                                    <td>
                                        <select name="purchased[]" id="purchased-0" class="form-control">
                                            <option value="yes">YES</option>
                                            <option value="no">NO</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="picked_up[]" id="picked_up-0" class="form-control">
                                            <option value="yes">YES</option>
                                            <option value="no">NO</option>
                                        </select>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-sm btn-danger" onclick="delete_form(0)"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-sm btn-info float-right" onclick="addForm()">Add more</button>
                    </div>
                    <hr class="mt-5 mb-5">
                    <div class="col-md-12">
                        <a href="<?= base_url('order/list') ?>" class="btn btn-sm btn-danger float-left">Back</a>
                        <button class="btn btn-sm btn-primary float-right" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</main>
</div>
<script>
    var formNumber = 1;

    function addForm() {
        var parent = document.querySelector("#table-form-order tbody");;
        var jumlahForm = document.getElementById('jumlah_form');
        var newChild = `
                            <tr id="form-order-${formNumber}">
                                <td>
                                    <input type="text" class="form-control" name="invoice[]" id="invoice-${formNumber}">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="no_order[]" id="no_order-${formNumber}">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="resi[]" id="resi-${formNumber}">
                                </td>
                                <td>
                                    <input type="date" class="form-control" name="chinas_port[]" id="china_port-${formNumber}">
                                </td>
                                <td>
                                    <input type="date" class="form-control" name="local_arrived[]" id="local_arrived-${formNumber}">
                                </td>
                                <td width="100px">
                                    <input type="date" class="form-control" name="date[]" id="date-${formNumber}">
                                </td>
                                <td>
                                    <select name="purchased[]" id="purchased-${formNumber}" class="form-control">
                                        <option value="yes">YES</option>
                                        <option value="no">NO</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="picked_up[]" id="picked_up-${formNumber}" class="form-control">
                                        <option value="yes">YES</option>
                                        <option value="no">NO</option>
                                    </select>
                                </td>
                                <td  class="text-center">
                                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_form(${formNumber})"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                    `;
        $('#table-form-order').find('tbody').append(newChild);
        formNumber++;
    }

    function delete_form(id) {
        var myobj = document.getElementById(`form-order-${id}`);
        myobj.remove();
        // $(`#form-order-${id}`).remove()
    }
</script>