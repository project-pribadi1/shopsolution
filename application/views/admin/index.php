<main>
    <div class="container-fluid p-5">
        <h3>Data</h3>
        <hr>
        <?= $this->session->flashdata('message'); ?>
        <div class="card">
            <div class="card-header">
                Form Search
            </div>
            <div class="card-body">
                <form action="<?= base_url('admin') ?>" method="POST">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="">Date</label>
                            <input type="date" name="date" id="date" class="form-control form-control-sm">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Invoice</label>
                            <input type="text" name="invoice" id="invoice1" class="form-control form-control-sm">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">No Order</label>
                            <input type="text" name="no_order" id="no_order1" class="form-control form-control-sm">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Resi</label>
                            <input type="text" name="resi" id="resi1" class="form-control form-control-sm">
                        </div>
                        <div class="form-group col-md-12">
                            <button class="btn btn-sm btn-primary float-right" type="submit">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <hr>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Order List
                <a href="<?= base_url('admin/form') ?>" class="btn btn-primary float-right btn-sm"><i class="fas fa-fw fa-plus"></i> Add Order</a>
                <a href="<?= base_url('admin') ?>" class="btn btn-success float-right btn-sm mr-3"><i class="fas fa-fw fa-sync-alt"></i> Refresh</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr class="text-center text-nowrap">
                                <th width="50px">#</th>
                                <th width="100px">No Order</th>
                                <th>Invoice</th>
                                <th>Resi</th>
                                <th>Date</th>
                                <th>China's Port</th>
                                <th>Local Arrived</th>
                                <th>Purchased</th>
                                <th>Picked up</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($orders as $key => $value) { ?>
                                <tr>
                                    <td class="text-center"><?= $key + 1 ?></td>
                                    <td><?= $value->no ?></td>
                                    <td><?= $value->invoice ?></td>
                                    <td><?= $value->resi ?></td>
                                    <td><?= ($value->date) ? date('d F Y', strtotime($value->date)) : '-' ?></td>
                                    <td><?= ($value->chinas_port) ? date('d F Y', strtotime($value->chinas_port)) : '-' ?></td>
                                    <td><?= ($value->local_arrived) ? date('d F Y', strtotime($value->local_arrived)) : '-' ?></td>
                                    <td class="text-center"><?= strtoupper($value->purchased) ?></td>
                                    <td class="text-center"><?= strtoupper($value->picked_up) ?></td>
                                    <td class="text-center">
                                        <a href="#" onclick="confirm_delete(<?= $value->id ?>)" title="Delete"><i class="fas fa-fw fa-trash"></i></a>
                                        <a href="#" data-id="<?= $value->id ?>" onclick="edit_order(<?= $value->id ?>)" data-toggle="modal" data-target="#modal-edit" title="Edit"><i class="fas fa-fw fa-pencil-alt"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form Edit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/update') ?>" id="form-order" method="post" enctype="multipart/form-data">
                <input type="hidden" id="id" name="id">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-6 col-md-6">
                            <label for="exampleInputEmail1">Invoice</label>
                            <input type="text" name="invoice" id="invoice" class="form-control form-control-sm" placeholder="Invoice">
                        </div>
                        <div class="form-group col-6 col-md-6">
                            <label for="exampleInputEmail1">Order No</label>
                            <input type="text" name="no_order" id="no_order" class="form-control form-control-sm" placeholder="Invoice">
                        </div>
                        <div class="form-group col-6 col-md-6">
                            <label for="exampleInputEmail1">Resi</label>
                            <input type="text" name="resi" id="resi" class="form-control form-control-sm" placeholder="Invoice">
                        </div>
                        <div class="form-group col-6 col-md-6">
                            <label for="exampleInputEmail1">Date</label>
                            <input type="date" name="date" id="date1" class="form-control form-control-sm" placeholder="Invoice">
                        </div>
                        <div class="form-group col-6 col-md-6">
                            <label for="exampleInputEmail1">China's Port</label>
                            <input type="date" name="chinas_port" id="chinas_port" class="form-control form-control-sm" placeholder="Invoice">
                        </div>
                        <div class="form-group col-6 col-md-6">
                            <label for="exampleInputEmail1">Local Arrived</label>
                            <input type="date" name="local_arrived" id="local_arrived" class="form-control form-control-sm" placeholder="Invoice">
                        </div>
                        <div class="form-group col-6 col-md-6">
                            <label for="exampleInputPassword1">Purchased</label>
                            <select name="purchased" id="purchased" class="form-control">
                                <option value="yes">YES</option>
                                <option value="no">NO</option>
                            </select>
                        </div>
                        <div class="form-group col-6 col-md-6">
                            <label for="exampleInputPassword1">Picked up</label>
                            <select name="picked_up" id="picked_up" class="form-control">
                                <option value="yes">YES</option>
                                <option value="no">NO</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function edit_order(id) {
        $.ajax({
            url: "<?= base_url() . 'admin/edit_order'; ?>",
            async: false,
            type: 'POST',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response) {
                console.log(response);
                $('#id').val(response.id);
                $('#no_order').val(response.no);
                $('#invoice').val(response.invoice);
                $('#resi').val(response.resi);
                $('#purchased').val(response.purchased);
                $('#chinas_port').val(response.chinas_port);
                $('#local_arrived').val(response.local_arrived);
                $('#date1').val(response.date);
                $('#picked_up').val(response.picked_up);
            }
        });
    }

    $(document).ready(function() {
        $(".table").DataTable();
    });

    function confirm_delete(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = `<?= base_url('order/delete/') ?>${id}`;
            }
        })
    }
</script>
<!-- /.content-wrapper -->