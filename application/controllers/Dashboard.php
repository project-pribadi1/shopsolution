<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('email')) {
            redirect('auth');
        }
        $this->load->model('order_', 'order');
    }

    public function index()
    {
        check_persmission_pages($this->session->userdata('group_id'), 'dashboard');
        $this->load->model('order_');
        $data['orders'] = $this->order->get_order()->result();
        $data['active'] = 'dashboard';
        $data['title'] = 'Dashboard';
        $data['subview'] = 'dashboard/view';
        $this->load->view('template/main', $data);
    }
}
