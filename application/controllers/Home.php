<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = null;
        $this->load->view('home/index', $data);
        // $this->load->view('home/home', $data);
    }

    public function tracking()
    {
        $search = $this->input->get('search');

        if ($search) {
            $this->db->select('*');
            $this->db->like('resi', $search);
            $this->db->or_like('no', $search);
            $this->db->or_like('invoice', $search);
            $hasil = $this->db->get('order')->result();
        } else {
            $hasil = [];
        }

        $data['hasil'] = $hasil;
        $this->load->view('home/tracking', $data);
    }

    function get_order()
    {
        $search = $this->input->post('search');

        $this->db->select('*');
        $this->db->like('resi', $search);
        $this->db->or_like('no', $search);
        $this->db->or_like('invoice', $search);
        $data = $this->db->get('order')->result();
        echo json_encode($data);
    }
}
