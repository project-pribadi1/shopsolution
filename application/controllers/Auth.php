<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('group_id')) {
            # code...
            if ($this->session->userdata('group_id') == 2) {
                redirect('member/listOrder');
            } else {
                redirect('dashboard');
            }
        }


        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('auth/login');
        } else {
            $this->_login();
        }
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $user = $this->db->get_where('users', ['email' => $email])->row_array();

        if ($user) {
            if ($user['is_active'] == 1) {
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'id' => $user['id'],
                        'email' => $user['email'],
                        'group_id' => $user['group_id'],
                        'name' => $user['name'],
                        'modal_show' => $user['modal_show'],
                    ];

                    $this->session->set_userdata($data);
                    redirect('admin');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Wrong password!</div>');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">This email is has been not activated!</div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Email not register!</div>');
            redirect('auth');
        }
    }

    function register()
    {
        if ($this->session->userdata('group_id')) {
            # code...
            if ($this->session->userdata('group_id') == 2) {
                redirect('member/listOrder');
            } else {
                redirect('dashboard');
            }
        }

        $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('telpon', 'Telpon', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');

        if ($this->form_validation->run() == false) {
            $this->load->view('auth/register');
        } else {
            $this->_registerMember();
        }
    }

    function _registerMember()
    {
        $email = htmlspecialchars($this->input->post('email'));
        $password = $this->input->post('password');
        $name = $this->input->post('full_name');
        $telp = $this->input->post('telpon');

        $data = [
            'name' => $name,
            'email' => $email,
            'password' => password_hash($password, PASSWORD_DEFAULT),
            'telpon' => str_replace("-", "", $telp),
            'group_id' => 2,
            'is_active' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ];

        if ($this->db->insert('users', $data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> Congratulations your acount has been created. Please Login</div>');
            redirect('auth');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Your acount failed created !!</div>');
            redirect('auth/register');
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">You have been logout!</div>');
        redirect('auth');
    }

    public function blocked()
    {
        $data['active'] = null;
        $data['title'] = 'Page Not Found';
        $data['subview'] = 'auth/not_found';
        $this->load->view('auth/not_found', $data);
    }

    function changeModalShow()
    {
        $data = [
            'modal_show' => $this->input->post('modalShow')
        ];
        $this->db->update('users', $data, ['id' => $this->session->userdata('id')]);
    }
}
