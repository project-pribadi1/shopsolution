<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('email')) {
            redirect('auth');
        }
        $this->load->model('member_');
    }

    function index()
    {
        $data['orders'] = $this->member_->get_order()->result();
        $data['active'] = 'member';
        $data['title'] = 'Home';
        $data['subview'] = 'member/index';
        $this->load->view('member/index', $data);
    }

    function listOrder()
    {
        check_persmission_pages($this->session->userdata('group_id'), 'member/listOrder');
        $data['orders'] = $this->member_->get_order()->result();
        $data['active'] = 'member/listOrder';
        $data['title'] = 'Order List';
        $data['subview'] = 'member/listOrder';
        $this->load->view('member/listOrder', $data);
    }

    function detailOrder($id)
    {
        // check_persmission_pages($this->session->userdata('group_id'), 'member/listOrder');
        $data['order'] = $this->member_->get_detail($id);
        $data['active'] = 'member/listOrder';
        $data['title'] = 'Order Detail';
        $data['subview'] = 'member/detail';
        $this->load->view('member/detail', $data);
    }

    function editOrder($id)
    {
        // check_persmission_pages($this->session->userdata('group_id'), 'member/listOrder');
        $data['order'] = $this->member_->get_edit($id);
        $data['active'] = 'member/detailOrder';
        $data['title'] = 'Order Detail';
        $data['subview'] = 'member/edit';
        $this->load->view('member/edit', $data);
    }

    function deleteOrder($id)
    {
        $this->db->trans_begin();
        $this->load->model('member_');
        $dataImage = $this->db->get_where('order_item', ['order_id' => $id])->result();
        $imagedelet = [];
        foreach ($dataImage as $key => $value) {
            $data_image = json_decode($value->image);
            for ($i = 0; $i < sizeof($data_image); $i++) {
                $imagedelet[] = $data_image[$i];
            }
        }
        $this->db->delete('orders', ['id' => $id]);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Order failed deleted!</div>');
        } else {
            $this->db->trans_commit();

            for ($i = 0; $i < sizeof($imagedelet); $i++) {
                unlink(FCPATH . 'assets/img/image-order/' . $imagedelet[$i]);
            }
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Order success deleted!</div>');
        }

        echo '<script> window.history.go(-1) </script>';
    }

    public function formOrder()
    {
        check_persmission_pages($this->session->userdata('group_id'), 'member/formOrder');
        $data['active'] = 'member/formOrder';
        $data['title'] = 'Form Order';
        $data['subview'] = 'member/form';
        $this->load->view('member/form', $data);
    }

    function createOrder()
    {

        $this->db->trans_begin();
        $dateOrder = date('Y-m-d');
        $jumlahForm = $this->input->post('jumlah_form');
        $timestamp = date('Y-m-d H:i:s');
        $userId = $this->session->userdata('id');


        $order = [
            'user_id' => $userId,
            'date' => $dateOrder,
            'status' => 1,
            'created_at' => $timestamp
        ];

        $this->db->insert('orders', $order);
        $orderId = $this->db->insert_id();

        // menampung name image upload;
        $image_name = [];
        $pembuka = "Shopsolution Order !!!! \n\n";
        $pembuka .= "From :" . get_user_name($this->session->userdata('id'));
        $data_send = `\n\n List Order :`;
        $no_order = 1;

        for ($i = 0; $i < $jumlahForm; $i++) {

            $link = $this->input->post('link-' . $i);
            $quantity = $this->input->post('quantity-' . $i);
            $description = $this->input->post('description-' . $i);

            if (!empty($link)) {
                $data_send .= "\nNo : " . $no_order++;
                $data_send .= "\nLink : " . $link;
                $data_send .= "\nQuantity : " . $quantity;
                $data_send .= "\nDescription : " . $description;
                $data_send .= "\n\n";

                $image = [];
                $upload_image = $_FILES['image-' . $i]['name'];

                // config upload
                $config['allowed_types']    = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                // $config['max_size']         = '5048';
                $config['upload_path']      = './assets/img/image-order';
                $config['encrypt_name']     = true;
                // $config['quality']          = '50%';
                $this->load->library('upload', $config);

                for ($j = 0; $j < sizeof($upload_image); $j++) {

                    if (!empty($_FILES['image-' . $i]['name'][$j])) {

                        // config upload image
                        $_FILES['file']['name'] = $_FILES['image-' . $i]['name'][$j];
                        $_FILES['file']['type'] = $_FILES['image-' . $i]['type'][$j];
                        $_FILES['file']['tmp_name'] = $_FILES['image-' . $i]['tmp_name'][$j];
                        $_FILES['file']['error'] = $_FILES['image-' . $i]['error'][$j];
                        $_FILES['file']['size'] = $_FILES['image-' . $i]['size'][$j];

                        if ($this->upload->do_upload('file')) {
                            $image[] = $this->upload->data('file_name');
                            $image_name[] = $this->upload->data('file_name');
                        } else {
                            echo $this->upload->display_errors();
                        }
                    }
                }

                $data = [
                    'order_id' => $orderId,
                    'link' => $link,
                    'quantity' => $quantity,
                    'description' => $description,
                    'image' => json_encode($image),
                    'created_at' => $timestamp
                ];
                $this->db->insert('order_item', $data);
                // $this->db->insert_batch('image', $image);
            }
        }



        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            for ($i = 0; $i < sizeof($image_name); $i++) {
                unlink(FCPATH . 'assets/img/image-order' . $image_name[$i]);
            }
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Order failed sumitted</div>');
            redirect('member/listOrder');
        } else {
            $this->db->trans_commit();
            $kirim = $pembuka . "\n" . $data_send . "\n";
            sendTelegram($kirim);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Order success sumitted, await our confirm by whatsapp</div>');
            redirect('member/listOrder');
        }
    }

    function updateOrder()
    {
        $this->db->trans_begin();


        // menampung name image upload;
        $image_name = [];

        $id = $this->input->post('id');
        $link = $this->input->post('link');
        $quantity = $this->input->post('quantity');
        $description = $this->input->post('description');
        $status = $this->input->post('status');


        $timestamp = date('Y-m-d H:i:s');



        $image = [];
        $upload_image = $_FILES['image']['name'];

        if (!empty($upload_image[0])) {
            // hapus data image sebelum edit
            $imageOrder = $this->db->get_where('order_item', ['id' => $id])->row();
            // log_r($imageOrder);
            // foreach ($imageOrder as $key => $value) {
            $images = json_decode($imageOrder->image);
            for ($i = 0; $i < sizeof($images); $i++) {
                unlink(FCPATH . 'assets/img/image-order/' . $images[$i]);
            }

            // $this->db->delete('image', ['order_id' => $id]);

            // config upload
            $config['allowed_types']    = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
            $config['upload_path']      = './assets/img/image-order';
            $config['encrypt_name']     = true;
            // $config['max_size']         = '5048';
            // $config['quality']          = '50%';
            $this->load->library('upload', $config);

            for ($j = 0; $j < sizeof($upload_image); $j++) {

                if (!empty($_FILES['image']['name'][$j])) {

                    // config upload image
                    $_FILES['file']['name'] = $_FILES['image']['name'][$j];
                    $_FILES['file']['type'] = $_FILES['image']['type'][$j];
                    $_FILES['file']['tmp_name'] = $_FILES['image']['tmp_name'][$j];
                    $_FILES['file']['error'] = $_FILES['image']['error'][$j];
                    $_FILES['file']['size'] = $_FILES['image']['size'][$j];

                    if ($this->upload->do_upload('file')) {
                        // $image[] = [
                        //     'order_id' => $id,
                        //     'image' => $this->upload->data('file_name'),
                        //     'updated_at' => $timestamp
                        // ];
                        $image_name[] = $this->upload->data('file_name');
                    } else {
                        echo $this->upload->display_errors();
                    }
                }
            }
        }
        $data['link'] = $link;
        $data['quantity'] = $quantity;
        $data['description'] = $description;
        $data['updated_at'] = $timestamp;

        if ($image_name) {
            $data['image'] = json_encode($image_name);
        }
        $this->db->update('order_item', $data, ['id' => $id]);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            for ($i = 0; $i < sizeof($image_name); $i++) {
                unlink(FCPATH . 'assets/img/image-order' . $image_name[$i]);
            }
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Failed updated!</div>');
            echo '<script> window.history.go(-2) </script>';
        } else {
            $this->db->trans_commit();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Success updated!</div>');
            echo '<script> window.history.go(-2) </script>';
        }
    }

    function changePassword()
    {
        $this->form_validation->set_rules('current_password', 'Current Password', 'trim|required');
        $this->form_validation->set_rules('new_password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[new_password]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Change Password';
            $this->load->view('member/change_password', $data);
        } else {
            $this->_updatePassword();
        }
    }

    function _updatePassword()
    {
        $id = $this->input->post('id');
        $current_password = $this->input->post('current_password');
        $password = $this->input->post('new_password');

        $user = $this->db->get_where('users', ['id' => $id])->row();

        if (password_verify($current_password, $user->password)) {
            $data = [
                'password' => password_hash($password, PASSWORD_DEFAULT),
                'updated_at' => date('Y-m-d H:i:s')
            ];

            if ($this->db->update('users', $data, ['id' => $id])) {
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> Your password has been changed. Please Login</div>');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Your password failed changed !!</div>');
            }
            redirect('member/changePassword');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Current password is wrong!</div>');
            redirect('member/changePassword');
        }
    }

    function download($id)
    {
        $this->load->library('pdf');

        $data['order'] = $this->member_->get_download($id);
        // log_r($data);
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = 'Shop-' . $data['order'][0]->member . '-' . $data['order'][0]->date . '.pdf';
        $this->pdf->load_view('download', $data);
        // $this->load->view('download', $data);
    }

    function random()
    {
        $this->load->helper('string');
        log_r(random_string('alnum', 16));
    }
}
