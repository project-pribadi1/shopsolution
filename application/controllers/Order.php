<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('email')) {
            redirect('auth');
        }
        $this->load->model('order_', 'order');
    }

    function list()
    {
        check_persmission_pages($this->session->userdata('group_id'), 'order/list');

        $data['orders'] = $this->order->get_order()->result();
        $data['active'] = 'home';
        $data['title'] = 'Order List';
        $data['subview'] = 'order/list';
        $this->load->view('template/main', $data);
    }

    function detail($id)
    {
        $this->load->model('member_');
        $data['order'] = $this->member_->get_detail($id);
        $data['active'] = 'order/list';
        $data['title'] = 'Order Detail';
        $data['subview'] = 'order/detail';
        $this->load->view('template/main', $data);
    }

    function edit($id)
    {
        // check_persmission_pages($this->session->userdata('group_id'), 'member/listOrder');
        $this->load->model('member_');
        $data['order'] = $this->member_->get_edit($id);
        $data['active'] = 'order/detail';
        $data['title'] = 'Edit Order';
        $data['subview'] = 'order/edit';
        $this->load->view('template/main', $data);
    }

    function edit_order()
    {
        $id = $this->input->post('id');
        $data = $this->db->get_where('order', ['id' => $id])->row();
        echo json_encode($data);
    }


    function delete($id)
    {
        $this->db->trans_begin();

        $this->db->delete('order', ['id' => $id]);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Order failed deleted!</div>');
        } else {
            $this->db->trans_commit();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Order success deleted!</div>');
        }

        echo '<script> window.history.go(-1) </script>';
    }

    public function form()
    {
        check_persmission_pages($this->session->userdata('group_id'), 'order/form');
        $data['active'] = 'form';
        $data['title'] = 'Form Order';
        $data['subview'] = 'order/form_new';
        $this->load->view('template/main', $data);
    }

    function create()
    {

        $this->db->trans_begin();

        $no = $this->input->post('no_order');
        $invoice = $this->input->post('invoice');
        $chinas_port = $this->input->post('chinas_port');
        $purchased = $this->input->post('purchased');
        $resi = $this->input->post('resi');
        $local_arrived = $this->input->post('local_arrived');
        $picked_up = $this->input->post('picked_up');
        // menampung name image upload;
        $data = [];
        $date = date('Y-m-d H:i:s');
        for ($i = 0; $i < sizeof($no); $i++) {
            $data[] = [
                'no' => $no[$i],
                'invoice' => $invoice[$i],
                'chinas_port' => $chinas_port[$i],
                'local_arrived' => $local_arrived[$i],
                'resi' => $resi[$i],
                'purchased' => $purchased[$i],
                'picked_up' => $picked_up[$i],
                'created_at' => $date
            ];
        }

        $this->db->insert_batch('order', $data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Order failed sumitted!</div>');
        } else {
            $this->db->trans_commit();
        }
        redirect('order/list');
    }

    function update()
    {
        $this->db->trans_begin();
        $id = $this->input->post('id');
        $no = $this->input->post('no_order');
        $invoice = $this->input->post('invoice');
        $chinas_port = $this->input->post('chinas_port');
        $purchased = $this->input->post('purchased');
        $resi = $this->input->post('resi');
        $local_arrived = $this->input->post('local_arrived');
        $picked_up = $this->input->post('picked_up');
        $data = [
            'no' => $no,
            'invoice' => $invoice,
            'chinas_port' => $chinas_port,
            'local_arrived' => $local_arrived,
            'resi' => $resi,
            'purchased' => $purchased,
            'picked_up' => $picked_up,
            'updated_at' => date('Y-m-d H:i:s')
        ];
        $this->db->update('order', $data, ['id' => $id]);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Failed updated!</div>');
        } else {
            $this->db->trans_commit();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Success updated!</div>');
        }
        redirect('order/list');
    }

    function form_new()
    {
        $data['orders'] = $this->order->get_order()->result();
        $this->load->view('new', $data);
    }
}
