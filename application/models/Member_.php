<?php

class Member_ extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_order()
    {
        $this->db->select('A.*');
        $this->db->where('A.user_id', $this->session->userdata('id'));
        return $this->db->get('orders A');
    }

    function get_detail($id)
    {
        $this->db->select('A.*, B.link,B.quantity, B.image, B.description, B.id as item_id,  C.name as member, C.telpon');
        $this->db->join('order_item B', 'A.id = B.order_id');
        $this->db->join('users C', 'A.user_id = C.id');
        $this->db->where('A.id', $id);
        return $this->db->get('orders A')->result();
        // $this->db->select('A.*, B.link,B.quantity, B.image, B.description, B.id as item_id,  C.name as member, C.telpon');
        // $this->db->join('order_item B', 'A.id = B.order_id');
        // $this->db->join('users C', 'A.user_id = C.id');
        // $this->db->where('A.user_id', $user_id);
        // $this->db->where('A.date', $date);
        // return $this->db->get('orders A')->result();
    }

    function get_edit($id)
    {
        $this->db->select('A.*');
        // $this->db->join('order_item B', 'A.id = B.order_id');
        // $this->db->join('users C', 'A.user_id = C.id');
        $this->db->where('A.id', $id);
        return $this->db->get('order_item A')->row();
    }

    function get_image($order_id)
    {
        $this->db->select('*');
        $this->db->where('order_id', $order_id);
        return $this->db->get('image')->result();
    }

    function get_download($id)
    {
        $this->db->select('A.*, B.link,B.quantity, B.image, B.description, B.id as item_id,  C.name as member, C.telpon');
        $this->db->join('order_item B', 'A.id = B.order_id');
        $this->db->join('users C', 'A.user_id = C.id');
        $this->db->where('A.id', $id);
        return $this->db->get('orders A')->result();
    }
}
