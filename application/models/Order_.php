<?php

use Svg\Tag\Group;

class Order_ extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_order($status = null)
    {
        $date = $this->input->post('date');
        $invoice = $this->input->post('invoice');
        $resi = $this->input->post('resi');
        $no_order = $this->input->post('no_order');

        $this->db->select('A.*');
        if ($date != null) {
            $this->db->like('A.created_at', $date);
        }
        if ($invoice != null) {
            $this->db->like('A.invoice', $invoice);
        }
        if ($resi != null) {
            $this->db->like('A.resi', $resi);
        }
        if ($no_order != null) {
            $this->db->like('A.no', $resi);
        }
        return $this->db->get('order A');
    }
}
